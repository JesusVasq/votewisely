import React, {PureComponent} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import County from '../components/state-counties';
import '../css/states.css';
import {nodelink} from '../api';

/* Material UI GridList*/
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

/* Materia UI Card */
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';


function titleCase(str) {
   var splitStr = str.toLowerCase().split(' ');
   for (var i = 0; i < splitStr.length; i++) {
       // You do not need to check if i is larger than splitStr length, as your for does that for you
       // Assign it back to the array
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
   }
   // Directly return the joined string
   return splitStr.join(' ');
}

function getStateCode (name) {
	switch(name) {
		case 'alaska': {
			return 'ak';
		}
		case 'alabama': {
			return 'al';
		}
		case 'arizona': {
			return 'az';
		}
		case 'arkansas': {
			return 'ar';
		}
		case 'california': {
			return 'ca';
		}
		case 'colorado': {
			return 'co';
		}
		case 'connecticut': {
			return 'ct';
		}
		case 'delaware': {
			return 'de';
		}
		case 'florida': {
			return 'fl';
		}
		case 'georgia': {
			return 'ga';
		}
		case 'hawaii': {
			return 'hi';
		}
		case 'idaho': {
			return 'id';
		}
		case 'illinois': {
			return 'il';
		}
		case 'indiana': {
			return 'in';
		}
		case 'iowa': {
			return 'ia';
		}
		case 'kansas': {
			return 'ks';
		}
		case 'kentucky': {
			return 'ky';
		}
		case 'louisiana': {
			return 'la';
		}
		case 'maine': {
			return 'me';
		}
		case 'maryland': {
			return 'md';
		}
		case 'massachusetts': {
			return 'ma';
		}
		case 'michigan': {
			return 'mi';
		}
		case 'minnesota': {
			return 'mn';
		}
		case 'mississippi': {
			return 'ms';
		}
		case 'missouri': {
			return 'mo';
		}
		case 'montana': {
			return 'mt';
		}
		case 'nebraska': {
			return 'ne';
		}
		case 'nevada': {
			return 'nv';
		}
		case 'new-hampshire': {
			return 'nh';
		}
		case 'new-jersey': {
			return 'nj';
		}
		case 'new-mexico': {
			return 'nm';
		}
		case 'new-york': {
			return 'ny';
		}
		case 'north-carolina': {
			return 'nc';
		}
		case 'north-dakota': {
			return 'nd';
		}
		case 'ohio': {
			return 'oh';
		}
		case 'oklahoma': {
			return 'ok';
		}
		case 'oregon': {
			return 'or';
		}
		case 'pennsylvania': {
			return 'pa';
		}
		case 'rhode-island': {
			return 'ri';
		}
		case 'south-carolina': {
			return 'sc';
		}
		case 'south-dakota': {
			return 'sd';
		}
		case 'tennessee': {
			return 'tn';
		}
		case 'texas': {
			return 'tx';
		}
		case 'utah': {
			return 'ut';
		}
		case 'vermont': {
			return 'vt';
		}
		case 'virginia': {
			return 'va';
		}
		case 'washington': {
			return 'wa';
		}
		case 'west-virginia': {
			return 'wv';
		}
		case 'wisconsin': {
			return 'wi';
		}
		case 'wyoming': {
			return 'wy';
		}
		default: return 'ERROR';
	}
};

export default class States extends PureComponent {

	constructor(props) {
		super(props);
		this.state = {
			stateData: [],
			issues_info: [],
			stateReps: [],
			stateSens: [],
			stateBills: [],
			billsToIssues: [],
			issueInformation: [],
			senData: [],
			repData: [],
			billsData: [],
			issuesData: [],
			loaded: false,
			allBillIssues:[],
			billsLoaded: false,
      width: window.innerWidth,
      columns: 0
		};
	}



  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  // make sure to remove the listener
  // when the component is not mounted anymore
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
    if(this.state.width <= 480)
      this.setState({columns: 2});
    else if(this.state.width <= 768)
      this.setState({columns: 3});
    else
      this.setState({columns: 4});
  };

	componentDidMount() {
		var name = this.props.match.params.states;
		var abbrev = String(getStateCode(name)).toUpperCase();
		fetch(nodelink + "/api/states/" + abbrev)
			.then(res => res.json())
			.then(res => res.response)
			.then(res => this.setState({stateData: res}))
			.then(res => this.setState({loaded: true}));

		fetch(nodelink + "/api/reps/senate/?state=" + abbrev)
			.then(res => res.json())
			.then(res => res.response)
			.then(rep => this.setState({stateSens: rep}))
			.then(rep => this.setData(this.state.stateSens, "Senate"));

		fetch(nodelink + "/api/reps/house/?state=" + abbrev)
			.then(res => res.json())
			.then(res => res.response)
			.then(rep => this.setState({stateReps: rep}))
			.then(rep => this.setData(this.state.stateReps, "House"));

		fetch(nodelink + "/api/bills2/?state=" + abbrev)
			.then(rep => rep.json())
			.then(rep => rep.response)
			.then(rep => this.setState({stateBills: rep}))
			.then(rep =>
				fetch(nodelink + "/api/issues2")
				.then(res => res.json())
				.then(res => res.response)
				.then(res => this.setState({issues_info: res[0]}))
			)
			.then(rep =>
				fetch(nodelink + "/api/bills2issues")
				.then(res => res.json())
				.then(res => res.response)
				.then(res => this.setState({billsToIssues: res}))
				.then(res => this.setState({allBillIssues: this.setAllBillsData()}))
			)
			.then(rep => this.setBillData(this.state.stateBills))
			.then(rep => this.setState({billsLoaded: true}))

      this.handleWindowSizeChange();
	}

	setAllBillsData(){
		var issues_for_all_bills = [];
		for(var i = 0; i < this.state.stateBills.length; i++){
			var current_bill_id = this.state.stateBills[i].bill_id;
			var current_bill_issues = this.getIssueByBill(current_bill_id);
			issues_for_all_bills.push(current_bill_issues);
		}
		return issues_for_all_bills;
	}

	getIssueByBill(billId){
		var issues = [];
		for(var i = 0; i < this.state.billsToIssues.length; i ++){
			var tempBill  = this.state.billsToIssues[i];
			if(tempBill.bill_id === billId){
				var issue_name = tempBill.issue;
				issue_name = titleCase(issue_name.replace(/_/g," "));
				issues.push(tempBill.issue);
			}
		}
		return issues;
	}

	getIssuePic(issue_name){
		for(var i = 0; i < this.state.issues_info.length; i++){
			if(issue_name === this.state.issues_info[i].route){
				var img_link = this.state.issues_info[i].icon;
				return img_link;
			}
		}
		return "";
	}

	getTitle(pulledData, i){
		if(pulledData.length > 0)
			return pulledData[i].title;
	}

	getName(pulledData, i){
		if(pulledData.length > 0)
			return pulledData[i].first_name + " " + pulledData[i].last_name;
	}

	getFacebook(pulledData, i){
		if(pulledData.length > 0)
			return pulledData[i].facebook_account;
	}

	getTwitter(pulledData, i){
		if(pulledData.length > 0)
			return pulledData[i].twitter_account;
	}

	getBirth(pulledData, i){
		if(pulledData.length > 0)
			return pulledData[i].date_of_birth;
	}

	getImage(pulledData, i){
		if(pulledData.length > 0)
			return pulledData[i].img_url;
	}

	setData(pulledData, wing) {
		var extData = [];
		for(var i = 0; i < pulledData.length; i++){
			var tempData = []
			var Name = this.getName(pulledData, i);
			var Title = this.getTitle(pulledData, i);
			var Facebook = this.getFacebook(pulledData, i);
			var Twitter = this.getTwitter(pulledData, i);
			var Image = this.getImage(pulledData, i);
			tempData[0] = Name;
			tempData[1] = Title;
			tempData[2] = Facebook;
			tempData[3] = Twitter;
			tempData[4] = Image;
			extData[i] = tempData;
		}
		if(wing === "Senate")
			this.setState({senData : extData});
		else if(wing === "House")
			this.setState({repData : extData});
	}

	setBillData(pulledData){
		var extData = [];
		for(var i = 0; i < pulledData.length; i++){
			var tempData = []
			var BillID = pulledData[i].bill_id;
			var Title = pulledData[i].title;
			var IssueName = this.state.allBillIssues[i][0];
			var BillURL = pulledData[i].congressdotgov_url;
			var ImageLink = this.getIssuePic(IssueName);
			tempData[0] = BillID;
			tempData[1] = Title;
			tempData[2] = IssueName;
			tempData[3] =  BillURL;
			tempData[4] = ImageLink;
			extData[i] = tempData;
		}
		this.setState({billsData : extData});
	}

	profileStyle = {
		height: '100%',
		width: 'auto',
		background: 'rgba(0, 0, 0, .3)',
	}

	renderProfile() {
		if(this.state.loaded){
			return (
				<Card style={{opacity: '.95'}}>
					<CardHeader
						title = {<h2 id="title">{this.state.stateData[0].name}</h2>}
						subheader = {<h4>{this.state.stateData[0].nickname}</h4>}
					/>
					<CardMedia
            image={this.state.stateData[0].banner_url}
						title={this.state.stateData[0].name}
						style={this.profileStyle}
					>
						<County state={this.state.stateData[0].name}/>

					</CardMedia>
					<CardContent>
					<div>
						<Container id="profile_block">
							<h5 className="profile-info" id="gov"> Governor: {this.state.stateData[0].governor_name}</h5>
							<h5 className="profile-info" id="capital"> Capital: {this.state.stateData[0].capital}</h5>
							<h5 className="profile-info"> Population: {this.state.stateData[0].population}</h5>
							<h5 className="profile-info"> Legislature: {this.state.stateData[0].legislature_name}</h5>
							<h5 className="profile-info"> Motto(s): {this.state.stateData[0].motto}</h5>
							<h5 className="profile-info"> State Flower: {this.state.stateData[0].state_flower}</h5>

						</Container>
					</div>
				</CardContent>
				</Card>
			);
		}
	}

	renderPeople() {
		return (
			<Container>
				<GridList id="grid_state_sen" cellHeight={200} cols={this.state.columns}>
					<GridListTile key="Subheader" cols={this.state.columns} style={{ height: 'auto' }}>
						<h2 id="grid-title">Senators</h2>
					</GridListTile>
					{this.state.senData.map(tile => (
						<GridListTile key={tile[0]}>
							<img src={tile[4]} alt={tile[0]}/>
							<GridListTileBar
								title={<h5 className="title">{tile[0]}</h5>}
								subtitle={<h5 className="subtitle">{tile[1]}</h5>}
								actionIcon={
									<IconButton color="primary" className="SenButton" href={"/politicians/" + String(tile[0]).replace(" ", "-").toLowerCase()}>
										<InfoIcon />
									</IconButton>
								}
							/>
						</GridListTile>
					))}
				</GridList>

				<br />
				<br />
				<br />
				{/* The House */}

				<GridList id="grid_state_rep" cellHeight={200} cols={this.state.columns}>
					<GridListTile key="Subheader" cols={this.state.columns} style={{ height: 'auto' }}>
						<h2 id="grid-title">Representatives</h2>
					</GridListTile>
					{this.state.repData.map(tile => (
						<GridListTile key={tile[0]}>
							<img src={tile[4]} alt={tile[0]}/>
							<GridListTileBar
								title={<h5 className="title">{tile[0]}</h5>}
								actionIcon={
									<IconButton id="RepLink" color="primary" href={"/politicians/" + String(tile[0]).replace(" ", "-").toLowerCase()}>
										<InfoIcon />
									</IconButton>
								}
							/>
						</GridListTile>
					))}
				</GridList>
			</Container>
		);
	}

  renderBills(){
    return (
      <Container>
        {/* The Bills */}
				<GridList id="grid_state_bill" cellHeight={150} cols={2}>
					<GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
						<h5 id="grid-title">State Bills</h5>
					</GridListTile>
					{this.state.billsData.map(tile => (
						<GridListTile key={tile[0]}>
							<img src={tile[4]} alt={tile[0]}/>
							<GridListTileBar
								title={<h6 className="title">{tile[1]}</h6>}
								subtitle={<a href={"/issues/"+tile[2]}><u style={{color: 'white'}}><h6 style={{textAlign: 'left', color: 'white'}}>{titleCase(tile[2].replace(/_/g," "))}</h6></u></a>}
								actionIcon={
									<IconButton color="primary" href={tile[3]}>
										<InfoIcon />
									</IconButton>
								}
							/>
						</GridListTile>
					))}

				</GridList>
      </Container>
    );
  }

	render () {

		var StateCode = getStateCode(this.props.match.params.states);
		var StateFlag = "https://www.50states.com/images/redesign/flags/"+ StateCode +"-largeflag.png"

		return (
				<Container className="flex-section" id="state-container" style={{height: '100%'}}>
					<img
						src = {StateFlag}
						className = "state-bg"
						alt = ""
					/>
					<Row >
						<Col xs={12} md={3} className="flex-col-scroll state-feed" id="section-profile">
							{this.renderProfile()}
						</Col>
						<Col xs={12} md={6} className="flex-col-scroll state-feed" id="section-newsfeed">
							{this.renderPeople()}
						</Col>
            <Col xs={12} md={3} className="flex-col-scroll state-feed" id="section-newsfeed">
              {this.renderBills()}
            </Col>
					</Row>
				</Container>

		);
	}
}
