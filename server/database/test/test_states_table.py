import mysql.connector
from mysql.connector import errorcode
import requests
import sys
import unittest
from unittest.mock import patch, Mock

sys.path.insert(0, ".")
import states_table
from apikey import apikey


class StatesTableTests(unittest.TestCase):
    @patch("requests.get")
    def test_get_request_queries_correctly(self, mock_method):
        mock_response = Mock()
        mock_response.status_code = 200
        mock_response.json = lambda: {"test_key": "test_result"}
        mock_method.return_value = mock_response

        test_res = states_table.get_request()

        self.assertEqual(test_res, mock_response.json())
        self.assertTrue(
            mock_method.called_once_with(
                url=f"https://openstates.org/api/v1/metadata?apikey=0d28c2ca-fd46-4c06-a241-cec8da7b08a0"
            )
        )

    @patch("requests.get")
    def test_get_request_raise_exception_on_error(self, mock_method):
        mock_response = Mock()
        mock_response.status_code = 404
        mock_response.json = lambda: {"test_key": "test_result"}
        mock_method.return_value = mock_response

        with self.assertRaises(Exception) as error:
            test_res = bills_table.get_request()
            self.assertEqual(error.msg, f"GET /tasks/ {mock_response.status_code}")

    @patch("states_table.cursor.execute")
    def test_create_table_calls_cursor_execute(self, mock_method):
        states_table.create_table("states_table")
        self.assertEqual(mock_method.call_count, 1)
