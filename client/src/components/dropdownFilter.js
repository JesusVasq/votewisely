import React, { PureComponent } from 'react';
import { Dropdown } from "react-bootstrap";

export default class DropdownFilter extends PureComponent {
  constructor(props) {
    /* Props args: list, onClick, defaultIndex (optional) */
    super(props);
    this.state = {
      index: this.props.defaultIndex
    };
  }

  render() {
    return (
      <div className="selector">
        {
          this.props.list.map((name, idx) =>
            <Dropdown.Item
                key={idx}
                onClick={() => {
                  if (this.state.index !== idx) {
                    this.props.onClick(name);
                    this.setState({
                      index: idx
                    });
                  }
                }}>
              {name}
            </Dropdown.Item>
          )
        }
      </div>
    );
  }
}
