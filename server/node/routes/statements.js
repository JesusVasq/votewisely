const express = require('express');
const helpers = require('../lib/sqlHelpers');

const router = express.Router();

const TABLE = 'statements_table';

function statementsSQLGenerator(req) {
  const args = [];
  let sql = `${'SELECT * '
        + 'FROM '}${TABLE}`;
  if (typeof req.query.id !== 'undefined' || typeof req.query.state !== 'undefined'
      || typeof req.query.member_id !== 'undefined') {
    sql += ' WHERE ';
    if (req.query.id) {
      sql += 'id=? AND ';
      args.push(req.query.id);
    }
    if (req.query.state) {
      sql += 'state=? AND ';
      args.push(req.query.state);
    }
    if (req.query.member_id) {
      sql += 'member_id=? AND ';
      args.push(req.query.member_id);
    }
    sql = sql.substring(0, sql.length - 4);
  }
  return [sql, args];
}

/**
 * Get all or some of the statements information by queries
 * /:   get all the statements information
 * /    ?id=1
 * /    ?state="CA"
 * /    ?member_id="D000435"
 *
 */
router.get('/', helpers.sqlRequestFactory(statementsSQLGenerator));

module.exports = {
  statementsSQLGenerator,
  router,
};
