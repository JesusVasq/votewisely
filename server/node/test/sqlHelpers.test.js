const chai = require('chai');

const { expect } = chai;
const sinon = require('sinon');
const helpers = require('../lib/sqlHelpers');

describe('SQL Helpers', () => {
  it('jsonResponseFactory generate a function that will use Express functions to return a json', () => {
    const mockRes = sinon.mock();
    mockRes.json = sinon.stub();
    const mockProcessFn = sinon.stub();
    const mockResults = ['testResult'];
    mockProcessFn.returns(mockResults);
    const testJSONfn = helpers.jsonResponseFactory(mockRes, mockProcessFn);
    expect(testJSONfn).to.be.a('function');

    testJSONfn(null, mockResults, null);
    expect(mockProcessFn.called).to.equal(true);
    expect(mockRes.json.called).to.equal(true);
    expect(mockRes.json.calledWith({
      status: 200,
      error: null,
      response: mockResults,
    })).to.equal(true);
  });

  it('jsonResponseFactory generate a function that will use Express functions to return a json for error', () => {
    const mockRes = sinon.mock();
    mockRes.json = sinon.stub();
    const mockProcessFn = sinon.stub();
    const mockResults = ['testResult'];
    mockProcessFn.returns(mockResults);
    const testJSONfn = helpers.jsonResponseFactory(mockRes, mockProcessFn);
    expect(testJSONfn).to.be.a('function');

    testJSONfn('testError', null, null);
    expect(mockProcessFn.called).to.equal(false);
    expect(mockRes.json.called).to.equal(true);
    expect(mockRes.json.calledWith({
      status: 500,
      error: 'testError',
      response: null,
    })).to.equal(true);
  });

  it('sqlRequestFactory generates a function that can be used to make a SQL request', () => {
    const mockSQLfn = sinon.mock();
    mockSQLfn.returns(['testSQLstatement', ['testArg1', 'testArg2']]);
    const processfn = sinon.mock();
    const mockReq = sinon.mock();
    const mockRes = sinon.mock();
    mockRes.locals = sinon.mock();
    mockRes.locals.connection = sinon.mock();
    mockRes.locals.connection.query = sinon.stub();
    mockRes.locals.connection.end = sinon.stub();

    const testSQLrequest = helpers.sqlRequestFactory(mockSQLfn, processfn);
    expect(testSQLrequest).to.be.a('function');

    testSQLrequest(mockReq, mockRes);
    const testArgs = mockRes.locals.connection.query.args[0];
    expect(testArgs[0]).to.equal('testSQLstatement');
    expect(testArgs[1]).to.be.an('array');
    expect(testArgs[1]).to.have.length(2);
    expect(testArgs[1][0]).to.equal('testArg1');
    expect(testArgs[1][1]).to.equal('testArg2');
    expect(testArgs[2]).to.be.a('function');

    expect(mockRes.locals.connection.end.args[0][0]).to.be.a('function');
  });
});
