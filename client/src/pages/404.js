import React, { PureComponent } from 'react';
import { Container } from 'react-bootstrap';
import Video from '../imgs/flag_waving.mp4';
import '../css/home.css';
/* GUI */

class Component404 extends PureComponent {
  render() {
    return (
			<div>
        <video id="background-video" loop autoPlay muted>
			    <source src={Video} type="video/mp4" />
			    <source src={Video} type="video/ogg" />
			    Your browser does not support the video tag.
				</video>
				<h1 id="splash-title">~~ 404 ~~</h1>
				<h2 id="splash-subtitle">Oops! This page does not exist...</h2>
			</div>
    );
  }
}

export default class Page404 extends PureComponent {
	render () {
		return (
			<Container id="home-container">
				<Component404 />
			</Container>
		);
	}
}
