const chai = require('chai');

const { expect } = chai;
const sinon = require('sinon');
const statements = require('../routes/statements');

describe('Statements', () => {
  it('statementsSQLGenerator should recieve queries to generate a SQL statement to get information about a statement', () => {
    const mockReq = sinon.mock();
    mockReq.query = sinon.mock();
    mockReq.query.id = 'testId';
    mockReq.query.state = 'testState';
    mockReq.query.member_id = 'testMemberId';
    const [testSQL, testSQLargs] = statements.statementsSQLGenerator(mockReq);
    expect(testSQL).to.equal('SELECT * FROM statements_table WHERE id=? AND state=? AND member_id=? ');
    expect(testSQLargs).to.be.an('array');
    expect(testSQLargs).to.have.length(3);
    expect(testSQLargs[0]).to.equal('testId');
    expect(testSQLargs[1]).to.equal('testState');
    expect(testSQLargs[2]).to.equal('testMemberId');
  });

  it('statesSQLGenerator should recieve no queries to generate a SQL statement to get information about all statements', () => {
    const mockReq = sinon.mock();
    mockReq.query = sinon.mock();
    mockReq.query.id = undefined;
    const [testSQL, testSQLargs] = statements.statementsSQLGenerator(mockReq);
    expect(testSQL).to.equal('SELECT * FROM statements_table');
    expect(testSQLargs).to.be.empty;
  });
});
