import React, {PureComponent} from 'react';
import * as d3 from "d3";

import { nodelink } from '../api';


function myCompare(a, b){
  var dict_val_1 = a.reps;
  var dict_val_2 = b.reps;
  return dict_val_2 - dict_val_1;
}

export default class ourVisual extends PureComponent{

  constructor(props) {
    super(props);
    this.state = {
      congress_members_raw: [],
      formatted_data: []
    };
  }

  sortReps(data){
    var state_dict = {};
    for(var i = 0; i < data.length; i++){
      if(data[i].state_fullname in state_dict)
        state_dict[data[i].state_fullname] += 1;
      else
        state_dict[data[i].state_fullname] = 1;
    }

    var state_array_with_dicts = [];
    for (var key in state_dict){
       var temp_dict = {};
       temp_dict["state"] = key;
       temp_dict["reps"] = state_dict[key];
       state_array_with_dicts.push(temp_dict);
    }
    this.setState({formatted_data: state_array_with_dicts.sort(myCompare)});
  }

  drawPop(data2){
    var margin = {top: 10, right: 30, bottom: 90, left: 40};
    var width = 1100 - margin.left - margin.right;
    var height = 600 - margin.top - margin.bottom;

    var svg = d3.select("#mysvg4")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform","translate(" + margin.left + "," + margin.top + ")");

    var data = data2;

    svg.data(data).enter();

    var x = d3.scaleBand()
      .range([ 0, width ])
      .domain(data.map(function(d) { return d.state; }))
      .padding(1);

    svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

    var y = d3.scaleLinear()
      .domain([0, 60])
      .range([ height, 0]);

    svg.append("g")
      .call(d3.axisLeft(y));

    svg.selectAll("myline")
      .data(data)
      .enter()
      .append("line")
      .attr("x1", function(d) {return x(d.state); })
      .attr("x2", function(d) {return x(d.state); })
      .attr("y1", function(d) {return y(d.reps); })
      .attr("y2", y(0))
      .attr("stroke", "grey");

    svg.selectAll("mycircle")
      .data(data)
      .enter()
      .append("circle")
      .attr("cx", function(d) { return x(d.state); })
      .attr("cy", function(d) { return y(d.reps); })
      .attr("r", "4")
      .style("fill", function(d) {return '#'+(Math.random()*0xFFFFFF<<0).toString(16)})
      .attr("stroke", "black")
      return svg.node();
  }

  componentDidMount() {
     fetch(nodelink + "/api/reps")
        .then(res => res.json())
        .then(res => res.response)
        .then(res => this.setState({congress_members_raw: res}))
        .then(res => this.sortReps(this.state.congress_members_raw))
        .then(res => this.drawPop(this.state.formatted_data));
  }
  render(){
    return (
      <div style={{background: "whitesmoke", paddingBottom: '40px', borderRadius: '10px', marginTop: '50px'}}>
                <br></br>
                <h5>Number of Reps per State for VoteWisely</h5>
          <svg id="mysvg4" width={this.state.width} height = {this.state.height}></svg>
      </div>
    );
  }
}
