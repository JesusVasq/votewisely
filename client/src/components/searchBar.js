import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';
import Popper from '@material-ui/core/Popper';
import Paper from '@material-ui/core/Paper';
import '../css/appBar.css';

export default class SearchBar extends PureComponent {

  state = {
    invalidSearch: false,
    anchor: null
  };

  static contextTypes = {
    router: PropTypes.object
  }

  getResults = data => {
    var searchTerm = String(data.target.value);
    var valid = this.validateSearch(searchTerm);
    if(valid){
      var newTerm = searchTerm.replace(/^[^A-Za-z0-9]+/gi, '');
      newTerm = newTerm.replace(/[^A-Za-z0-9]+/gi, ' ');
      this.setState(state => ({
        anchor: null,
        invalidSearch: false,
      }));
      this.props.searchFunction(newTerm);
    }
    else{
      const targ = data.currentTarget;
      this.setState(state => ({
        anchor: targ,
        invalidSearch: true,
      }));
    }
  }

  validateSearch(search){
    if(search.length > 0){
      for(var char in search){
        var code = search.charCodeAt(char);
        if ((code > 47 && code < 58) || // numeric (0-9)
            (code > 64 && code < 91) || // upper alpha (A-Z)
            (code > 96 && code < 123)) { // lower alpha (a-z)
          return true;
        }
      }
    }
    return false;
  }

  render() {
    return(
      <div className="">
        <Input
            disableUnderline={true}
            placeholder={this.props.placeholder}
            classes={{root: this.props.className}}
            id="global-input"
            margin="none"
            variant="filled"
            type="search"
            onKeyPress={(e)=>{
              if(e.key === "Enter")
                this.getResults(e)
            }}
            onBlur={()=>{this.setState({invalidSearch: false})}}
            />
          <Popper
            id="invalidSearch"
            open={this.state.invalidSearch}
            anchorEl={this.state.anchor}
            placement="left"
            >
            <Paper>
              <p className="popper-invalid">Invalid Search</p>
            </Paper>
          </Popper>
      </div>
    );
  }
}
