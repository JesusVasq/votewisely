import React, { PureComponent } from 'react';
import { Pagination } from 'react-bootstrap';
import { Dropdown, DropdownButton } from "react-bootstrap";

import { nodelink } from '../api';
import IssueTile from '../components/issueTile';
import SearchResults from '../components/searchResults';
import Selector from '../components/selector';
import DropdownFilter from '../components/dropdownFilter';
import Loading from '../imgs/cat_loading_trans.gif';
import '../css/issues.css';

export default class Issues extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      active: 1,
      issuesPerPage: 9,
      searchInput: '',
      issuesConfig: {
        alphabetFilter: 'None',
        categoryFilter: 'None',
        votesFilter: 'None',
        alphabetSort: 'Ascending'
      },
      onSearch: false,
      searchQuery: null
    };
    this.scroll = false;
    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleAlphabetFilter = this.handleAlphabetFilter.bind(this);
    this.handleCategoryFilter = this.handleCategoryFilter.bind(this);
    this.handleVotesFilter = this.handleVotesFilter.bind(this);
    this.handleSort = this.handleSort.bind(this);
  }

  componentDidMount() {
    fetch(nodelink + '/api/issues2/')
      .then(res => res.json())
      .then(res => this.setState({
        issues: res.response[0],
        categories: ['None'].concat(res.response[1])
      }))
      .then(() => this.issuesUpdate(this.state.issuesConfig));
  }

  componentDidUpdate() {
    if (this.scroll) {
      this.bottomOfPage.scrollIntoView({ behavior: 'smooth' });
      this.scroll = false;
    }
  }

  handleSelect(idx) {
    if (this.state.active !== idx) {
      this.scroll = true;
      this.setState({
        active: idx
      });
    }
  }

  handleInput(e) {
    this.setState({ searchInput: e.target.value });
  }

  handleSubmit(e) {
    if (this.state.searchInput.length > 0) {
      this.setState({
        searchInput: '',
        searchQuery: this.state.searchInput,
        onSearch: true
      });
    }
    e.preventDefault();
  }

  handleReset() {
    if (this.state.searchInput.length > 0) {
      this.setState({ searchInput: '' });
    }
    this.issuesUpdate({
      alphabetFilter: 'None',
      categoryFilter: 'None',
      votesFilter: 'None',
      alphabetSort: 'Ascending'
    });
    this.setState({
      onSearch: false,
      searchQuery: null,
      active: 1
    })
  }

  handleAlphabetFilter(alphabet) {
    var newConfig = Object.assign({}, this.state.issuesConfig);
    newConfig.alphabetFilter = alphabet;
    this.issuesUpdate(newConfig);
  }

  handleCategoryFilter(category) {
    var newConfig = Object.assign({}, this.state.issuesConfig);
    newConfig.categoryFilter = category;
    this.issuesUpdate(newConfig);
  }

  handleVotesFilter(votesRange) {
    var newConfig = Object.assign({}, this.state.issuesConfig);
    newConfig.votesFilter = votesRange;
    this.issuesUpdate(newConfig);
  }

  handleSort(type) {
    var newConfig = Object.assign({}, this.state.issuesConfig);
    newConfig.alphabetSort = type;
    this.issuesUpdate(newConfig);
  }

  issuesUpdate(config) {
    var newIssues = this.state.issues;
    var active = this.state.active;
    if (config.alphabetFilter !== 'None') {
      newIssues = newIssues.filter(issue => issue.name.charAt(0) === config.alphabetFilter);
    }
    if (config.categoryFilter !== 'None') {
      newIssues = newIssues.filter(issue => issue.category === config.categoryFilter);
    }
    if (config.votesFilter !== 'None') {
      var sides = config.votesFilter.split('-');
      if (config.votesFilter.charAt(0) === '>') {
        sides = config.votesFilter.split('>');
        sides[0] = sides[1]
        sides[1] = 99999999
      }
      newIssues = newIssues.filter(issue => issue.votes_total > sides[0] && issue.votes_total < sides[1]);
    }
    if (config.alphabetSort === 'Ascending') {
      newIssues.sort((a, b) => a.name.localeCompare(b.name));
    } else if (config.alphabetSort === 'Descending') {
      newIssues.sort((a, b) => b.name.localeCompare(a.name));
    }
    if (active - 1 > newIssues.length / this.state.issuesPerPage) {
      active = 1;
    }
    this.setState({
      active: active,
      issuesConfig: config,
      issuesOnDisplay: newIssues,
    });
  }

  renderSearch() {
    return (
      <form className="search form-row align-items-center" onSubmit={this.handleSubmit}>
        <div className="col-auto">
          <label className="sr-only">Issues Search</label>
          <input type="text" className="form-control mb-2" placeholder="Search Issues" value={this.state.searchInput} onChange={this.handleInput}></input>
        </div>
        <div className="col-auto">
          <button type="submit" className="btn btn-primary mb-2">Submit</button>
          <button type="button" onClick={this.handleReset} className="btn btn-primary mb-2" id="reset-button">Reset</button>
        </div>
      </form>
    );
  }

  renderSearchResults() {
    return (
      <div>
        <SearchResults type='issues' query={this.state.searchQuery} />
      </div>
    );
  }

  renderSortBlock() {
    return (
      <div id="sort-block">
        <p id="sort-label">Sort:</p>
        <Selector
          list={['Ascending', 'Descending']}
          onClick={this.handleSort}
          defaultIndex={0} />
      </div>
    );
  }

  renderAlphabetDropdown() {
      return (
        <DropdownButton id="dropdown-basic-button" title={"Filter by Alphabet: " + this.state.issuesConfig.alphabetFilter}>
          <DropdownFilter
            list={['None', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
                  'I', 'L', 'M', 'N', 'O', 'P', 'R',
                  'S', 'T', 'U', 'V', 'W', 'Y']}
            onClick={this.handleAlphabetFilter}
            defaultIndex={0} />
        </DropdownButton>
      );
  }

  renderCategoryDropdown() {
      return (
        <DropdownButton id="dropdown-basic-button" title={"Filter by Category: " + this.state.issuesConfig.categoryFilter}>
          <DropdownFilter
            list={this.state.categories}
            onClick={this.handleCategoryFilter}
            defaultIndex={0} />
        </DropdownButton>
      );
  }

  renderVotesDropdown() {
      return (
        <DropdownButton id="dropdown-basic-button-support" className="dropdown-menu-support" title={"Filter by Total Votes: " + this.state.issuesConfig.votesFilter}>
          <DropdownFilter
            list={['None', '1-99999', '100000-999999', '1000000-9999999', '>10000000']}
            onClick={this.handleVotesFilter}
            defaultIndex={0} />
        </DropdownButton>
      );
  }

  renderAlphabetFilter() {
    return (
      <div className='alphabet-selector'>
        <Dropdown
          list={['All', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']}
          onClick={this.handleAlphabetFilter}
          defaultIndex={0} />
      </div>
    );
  }

  renderTiles() {
    const start = (this.state.active - 1) * this.state.issuesPerPage;
    const end = start + this.state.issuesPerPage;
    return (
      <div className="container" id="issues-list">
        <div className="row">
          {this.state.issuesOnDisplay.slice(start, end).map((issue, idx) =>
            <div key={"tile" + (start + idx)} className="col-xs-6 col-md-4 even-columns">
              <IssueTile
                data = {issue}
                highlight={this.state.highlight} />
            </div>
          )}
        </div>
      </div>
    );
  }

  renderPagination() {
    const amountOfPages = Math.ceil(this.state.issuesOnDisplay.length / this.state.issuesPerPage);
    var pages = [];
    if (this.state.active - 2 > 1) {
      pages.push(
        <Pagination.Item
          key={"..<"}
          onClick={() => this.handleSelect(this.state.active - 3)}>
            {".."}
        </Pagination.Item>
      );
    }
    for (let page = Math.max(this.state.active - 2, 1); page <= Math.min(this.state.active + 2, amountOfPages); page++) {
      pages.push(
        <Pagination.Item
          key={"pg" + page}
          active={page === this.state.active}
          onClick={() => this.handleSelect(page)}>
            {page}
        </Pagination.Item>
      );
    }
    if (this.state.active + 2 < amountOfPages) {
      pages.push(
        <Pagination.Item
          key={"..>"}
          onClick={() => this.handleSelect(this.state.active + 3)}>
            {".."}
        </Pagination.Item>
      );
    }

    return (
      <Pagination id="paging">
        <Pagination.First
          disabled={1 >= this.state.active}
          onClick={() => this.handleSelect(1)} />
        <Pagination.Prev
          disabled={1 >= this.state.active}
          onClick={() => this.handleSelect(this.state.active - 1)} />
        {pages}
        <Pagination.Next
          disabled={amountOfPages <= this.state.active}
          onClick={() => this.handleSelect(this.state.active + 1)} />
        <Pagination.Last
          disabled={amountOfPages <= this.state.active}
          onClick={() => this.handleSelect(amountOfPages)} />
      </Pagination>
    );
  }

  render() {
    if (this.state.issuesOnDisplay) {
      if (this.state.onSearch === false) {
        return (
          <div>
            <div className="container" id="issues-container">
              <div id="top-bar">
                {this.renderSearch()}
                {this.renderSortBlock()}
              </div>
              <div id="filters">
                {this.renderAlphabetDropdown()}
                {this.renderCategoryDropdown()}
                {this.renderVotesDropdown()}
              </div>
              {this.renderTiles()}
              <br />
              {this.renderPagination()}
            </div>
            <div ref={ref => this.bottomOfPage = ref} />
          </div>
        );
      } else {
        return (
          <div>
            <div className="container" id="issues-container">
              <div id="top-bar">
                {this.renderSearch()}
              </div>
              {this.renderSearchResults()}
            </div>
          </div>
        );
      }
    }
    return (
      <div>
        <div className="container" id="issues-container">
            <img src={Loading} alt="loading"></img>
            <h1 style={{margin: '0', paddingBottom: "1em"}}>Loading...</h1>
        </div>
        <div ref={ref => this.bottomOfPage = ref} />
      </div>
    );
  }
}
