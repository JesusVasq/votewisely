import React, { PureComponent } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import MemberCard from '../components/memberCard';
import '../css/about.css';

/* Member Images */
import ProfileJezze from '../imgs/members/jezze_gif.gif';
import ProfileLisa from '../imgs/members/lisa_gif.gif';
import ProfileAmiti from '../imgs/members/amiti.jpg';
import ProfileJay from '../imgs/members/jay_gif.gif';
import ProfileFan from '../imgs/members/fan_gif.gif';
import ProfileJesus from '../imgs/members/jesus_gif.gif';

/* Toolkit Images */
import GitLabLogo from '../imgs/gitlab-logo-gray-stacked-rgb.png';
import SlackLogo from '../imgs/slack-animation.gif';
import ReactLogo from '../imgs/React-logo-1.png';
import AWSLogo from '../imgs/PoliticalMindlessBanteng-small.gif';
import PSLogo from '../imgs/photoshop-full-logo.png';
import GimpLogo from '../imgs/wilber-big.png';
import PythonLogo from '../imgs/main-qimg-28cadbd02699c25a88e5c78d73c7babc.png';
import BSLogo from '../imgs/Bootstrap-Logo.png';
import PMLogo from '../imgs/pm-logo-vert.png';
import GoDaddyLogo from '../imgs/godaddylogo-new.png';
import MySQLLogo from '../imgs/MySQL.svg';
import DockerLogo from '../imgs/docker_facebook_share.png';
import SeleniumLogo from '../imgs/big-logo.png';
import GDriveLogo from '../imgs/logo-drive.png';
import NodeLogo from '../imgs/nodejs-new-pantone-black.png';
import JestEnzymeLogo from '../imgs/1_xWtpIKyjAVuWAlz_HjCkFA.jpeg';
import HelmetLogo from '../imgs/logo.svg';
import ReactBSLogo from '../imgs/1_36D6oCrl2Fpif_8NzK2lYA.png';
import ReactRouterLogo from '../imgs/1_uwSAzkmaJGFf_0GmvTTZRQ.png';
import MUILogo from '../imgs/download.png';
import D3Logo from '../imgs/d3.png';
import ExpressLogo from '../imgs/express-routing-logo-65137ed3c844d05124dcfdab28263c21-ec9c1.png';
import ChaiLogo from '../imgs/download.jpg';
import MochaLogo from '../imgs/68747470733a2f2f636c6475702e636f6d2f78465646784f696f41552e737667.svg';
import SinonLogo from '../imgs/6570253.png';
import ESLintLogo from '../imgs/6019716.png';
import NginxLogo from '../imgs/NGINX-logo-rgb-large.png';
import ComodoLogo from '../imgs/download (1).png';
import PM2Logo from '../imgs/pm2.20d3ef.png';
import AzureLogo from '../imgs/1280px-Microsoft_Azure_Logo.svg.png';
import GoogleLogo from '../imgs/google-developers-logo-png-event-details-2729.png';
import TwitterLogo from '../imgs/10wmt-articleLarge-v4.jpg';

/* Sources Images */
import OpenStatesLogo from '../imgs/openstates_logo.d5ae3c5f606e.svg';
import ProPublicaLogo from '../imgs/logo-propublica.svg';
import MediaWikiLogo from '../imgs/download (2).jpg';
import iSideWithLogo from '../imgs/117a4e4936ebaa96ab1557933d82f114.webp';

var commits = [];
var issues = {};

/* GUI */

class Purpose extends React.PureComponent {
  render() {
    return (
      <div id="purpose-container">
        <h2 className="title-text">Our Mission</h2>
        <p id="mission-statement">
          "Our Mission is to help citizens be politically informed
          and get excited to vote by providing them with unbiased
          information on politicians and issues based on their location"
        </p>
      </div>
    );
  }
}

class MemberList extends PureComponent {
	render() {
		return(
			<Container id="members-list" fluid={true}>
        <h2 className="title-text">Meet the Engineers</h2>
        <Row noGutters={false}>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Jesse Martinez"
              job="Front-end Engineer"
              class="5th-Year CS Student"
              bio="Really into gaming and all things 'geek'.
                   Still not over Endgame and
                   Episode 3 of Season 8 of Game of Thrones"
              image={ProfileJezze}
              picStyle={'selfie'}
              commits={133}
              issues={79}
              tests={12}
            />
          </Col>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Lisa Barson"
              job="Front-end Engineer"
              class="Junior CS Student"
              bio="Enjoys to play volleyball and go hiking!"
              image={ProfileLisa}
              picStyle='selfie'
              commits={73}
              issues={27}
              tests={12}
            />
          </Col>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Amiti Busgeeth"
              job="Front-end Engineer"
              class="Junior CS Student"
              bio="Cats and kittens are adorable!"
              image={ProfileAmiti}
              picStyle='selfie-jay'
              commits={45}
              issues={31}
              tests={10}
            />
          </Col>
        </Row>
        <Row noGutters={false}>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Jie Hao Liao"
              job="Full-stack Engineer"
              class="Junior CS Student"
              bio="Loving that Chipotle Lobster. And K-Pop. More K-Pop."
              image={ProfileJay}
              picStyle='selfie-jay'
              commits={138}
              issues={59}
              tests={24}
            />
          </Col>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Fan Yang"
              job="Back-end Engineer"
              class="Junior CS Student"
              bio="People say he can even be seen around campus riding on his 'Hoverboard'....
                   What a guy."
              image={ProfileFan}
              picStyle='selfie'
              commits={36}
              issues={35}
              tests={84}
            />
          </Col>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Jesus Vasquez"
              job="Back-end Engineer"
              class="Senior CS Student"
              bio="Game of Thrones is life. That is pretty much it."
              image={ProfileJesus}
              picStyle='selfie'
              commits={33}
              issues={21}
              tests={14}
            />
          </Col>
        </Row>
      </Container>
		);
	}
}

class Stats extends PureComponent {
  constructor(props) {
    super(props);
    this.contributors = {
      "Fan Yang": {
        name: "Fan Yang",
        username: "BertrandsParadogs",
        unitTests: 92
      },
      "JesusVasq": {
        name: "Jesus Vasquez",
        username: "JesusVasq",
        unitTests: 14
      },
      "AmitiB": {
        name: "Amiti Busgeeth",
        username: "AmitiB",
        unitTests: 10
      },
      "Lisa Barson": {
        name: "Lisa Barson",
        username: "lisab",
        unitTests: 12
      },
      "Jesse Martinez": {
        name: "Jezze Martinez",
        username: "Jezze4",
        unitTests: 16
      },
      "Jie Hao Liao": {
        name: "Jie Hao Liao",
        username: "liaojh1998",
        unitTests: 40
      }
    };
    this.state = {
      done: false
    };
  }

  componentDidMount() {
    fetch("https://gitlab.com/api/v4/projects/11046118/repository/contributors")
      .then(res => res.json())
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          const contributor = res[i];
          const data = {};
          if (this.contributors[contributor.name]) {
            data.name = this.contributors[contributor.name].name;
            data.username = this.contributors[contributor.name].username;
            data.commits = contributor.commits;

            commits[i] = data.commits;

            const state = {};
            state[data.username] = data;
            this.setState(state);
          }
        }
        return res;
      })
      .then(res => {
        this.setState({ done: true });
        for (let i = 0; i < res.length; i++) {
          const contributor = res[i];
          const username = this.contributors[contributor.name].username;
          fetch("https://gitlab.com/api/v4/users?username=" + username)
            .then(res => res.json())
            .then(res => res[0].id)
            .then(id => {
              fetch("https://gitlab.com/api/v4/projects/11046118/issues?assignee_id=" + id)
                .then(res => {
                  const data = Object.assign({}, this.state[username]);
                  data.issues = Number(res.headers.get('X-Total'));
                  issues[username] = data.issues;
                  const state = {};
                  state[username] = data;
                  this.setState(state);
                });
            });
        }
      });
  }

  totalCommits() {
    const commits = Object.values(this.contributors).reduce(
      (total, contributor) => total + this.state[contributor.username].commits, 0);
    return commits;
  }

  totalIssues() {
    if (Object.values(this.contributors).every((contributor) =>
        this.state[contributor.username].issues > 0)) {
      const issues = Object.values(this.contributors).reduce(
        (total, contributor) => total + this.state[contributor.username].issues, 0);
      return issues;
    }
    return 0;
  }

  totalUnitTests(){
    var total = 0;
    for (var key in this.contributors){
      var user = this.contributors[key];
      total = total + user.unitTests;
    }
    return total;
}

  renderTable() {
    if (this.state.done) {
      return (
        <Container>
          <table className="table table-cover">
            <thead>
              <tr>
                <th scope="col">Names</th>
                <th scope="col">Commits</th>
                <th scope="col">Assigned Issues</th>
                <th scope="col">Unit Tests</th>
              </tr>
            </thead>
            <tbody>
              {Object.values(this.contributors).map((contributor, idx) => (
                <tr key={"stats" + idx} className={"about-stat-row-" + idx}>
                  <td className="about-stat">{this.state[contributor.username].name}</td>
                  <td className="about-stat">{this.state[contributor.username].commits}</td>
                  <td className="about-stat">{this.state[contributor.username].issues}</td>
                  <td className="about-stat">{contributor.unitTests}</td>
                </tr>
              ))}
              <tr className="about-stat-row">
                <td className="about-stat"><strong>Total</strong></td>
                <td className="about-stat">{this.totalCommits()}</td>
                <td className="about-stat">{this.totalIssues()}</td>
                <td className="about-stat">{this.totalUnitTests()}</td>
              </tr>
            </tbody>
          </table>
        </Container>
      );
    }
    return (
      <div>
        <h3>Loading...</h3>
      </div>
    );
  }

  render() {
    return (
      <Container id="stats-container">
        <h2 className="title-text">The Statistics</h2>
        {this.renderTable()}
      </Container>
    );
  }
}

class ProjectLinks extends PureComponent {
  render(){
    return(
        <Container id="sources-container">
          <h2 className="title-text">Project Links</h2>
          <p>
            <a href="https://gitlab.com/votewiselyengineers/votewisely" target="_blank" rel="noopener noreferrer">
              GitLab
            </a>
          </p>
          <p>
            <a href="https://documenter.getpostman.com/view/6804368/S11NMwfE" target="_blank" rel="noopener noreferrer">
              Postman
            </a>
          </p>
        </Container>
    );
  }
}

class Toolkit extends PureComponent {
	render(){
    const tools = [
      {
        name: 'GitLab',
        logo: GitLabLogo,
        description: 'Version Control and open-source website for development.'
      },{
        name: 'Slack',
        logo: SlackLogo,
        description: 'Facilitates project communication and scheduling.'
      },{
        name: 'React.js',
        logo: ReactLogo,
        description: 'Front-end framework for efficient UI/UX designs.'
      },{
        name: 'AWS',
        logo: AWSLogo,
        description: 'Website host server.'
      },{
        name: 'Photoshop',
        logo: PSLogo,
        description: 'Making the banner and page background photos.'
      },{
        name: 'GIMP',
        logo: GimpLogo,
        description: 'Editing out the white parts of a gif.'
      },{
        name: 'Python',
        logo: PythonLogo,
        description: 'Back-end scripts for database modifications and insertions.'
      },{
        name: 'Bootstrap',
        logo: BSLogo,
        description: 'Front-end framework for pretty element designs.'
      },{
        name: 'Postman',
        logo: PMLogo,
        description: 'Documentation and testing for back-end API.'
      },{
        name: 'GoDaddy',
        logo: GoDaddyLogo,
        description: 'Domain name for website host server.'
      },{
        name: 'MySQL',
        logo: MySQLLogo,
        description: 'Back-end SQL database.'
      },{
        name: 'Docker',
        logo: DockerLogo,
        description: 'Continuous deployment containers.'
      },{
        name: 'Selenium',
        logo: SeleniumLogo,
        description: 'Graphics UI testing framework.'
      },{
        name: 'Google Drive',
        logo: GDriveLogo,
        description: 'Team report documents and Powerpoint presentations.'
      },{
        name: 'Node.js',
        logo: NodeLogo,
        description: 'Back-end API server.'
      },{
        name: 'Enzyme with Jest',
        logo: JestEnzymeLogo,
        description: 'Front-end testing framework for React.'
      },{
        name: 'Helmet',
        logo: HelmetLogo,
        description: 'Protection against web application vulnerabilities.'
      },{
        name: 'React Bootstrap',
        logo: ReactBSLogo,
        description: 'Front-end framework for pretty element designs, used in conjuction with Bootstrap for compatibility wiih React.'
      },{
        name: 'React Router',
        logo: ReactRouterLogo,
        description: 'Front-end website routing framework.'
      },{
        name: 'Material UI',
        logo: MUILogo,
        description: 'Front-end framework for data presentation in tables.'
      },{
        name: 'D3',
        logo: D3Logo,
        description: 'Front-end framework for data visualizations.'
      },{
        name: 'Express.js',
        logo: ExpressLogo,
        description: 'Web application framework.'
      },{
        name: 'Chai.js',
        logo: ChaiLogo,
        description: 'Back-end behaviour-driven development testing framework.'
      },{
        name: 'Mocha',
        logo: MochaLogo,
        description: 'Back-end testing facilitator.'
      },{
        name: 'Sinon.js',
        logo: SinonLogo,
        description: 'Back-end testing framework with mocks, stubs, and spies.'
      },{
        name: 'ESLint',
        logo: ESLintLogo,
        description: 'JavaScript linter for pretty and correct coding styles.'
      },{
        name: 'Nginx',
        logo: NginxLogo,
        description: 'Reverse proxy for website load balancing, redirects, and secure connections.'
      },{
        name: 'Comodo',
        logo: ComodoLogo,
        description: 'Source for website SSL certificates.'
      },{
        name: 'PM2',
        logo: PM2Logo,
        description: 'Server production process manager.'
      }
    ];
		return(
				<Container id="sources-container">
					<h2 className="title-text">Our Toolbelt</h2>
          <Row className="center-row">
            {tools.map((tool, idx) =>
              <Col xs={6} md={4}>
                <img
                  src={tool.logo}
                  alt={tool.name}
                  className="logo"
                />
                <div className="logoText">
                  <p><b>{tool.name}</b></p>
                  <p>{tool.description}</p>
                </div>
              </Col>
            )}
					</Row>
				</Container>
		);
	}
}

class Sources extends PureComponent {
  render(){
    const sources = [
      {
        name: 'ProPublica Congress API',
        logo: ProPublicaLogo,
        description: 'Information for recent bills, statements, and congress members.'
      },{
        name: 'MediaWiki API',
        logo: MediaWikiLogo,
        description: 'Congress members biography and states information on Wikipedia.'
      },{
        name: 'Open States API',
        logo: OpenStatesLogo,
        description: 'States information such as population and capital.'
      },{
        name: 'iSideWith.com',
        logo: iSideWithLogo,
        description: 'Pro/con debates on controversial issue topics and public opinions.'
      },{
        name: 'GitLab API',
        logo: GitLabLogo,
        description: 'Member contribution information.'
      },{
        name: 'Microsoft Azure: Bing Image Search API',
        logo: AzureLogo,
        description: 'Related images for controversial issues.'
      },{
        name: 'Google Developers: Youtube Data API',
        logo: GoogleLogo,
        description: 'Related videos for controversial issues.'
      },{
        name: 'Twitter Embed',
        logo: TwitterLogo,
        description: 'Congress members\' twitter updates.'
      }
    ];
    return(
        <Container id="sources-container">
          <h2 className="title-text">Our Data Sources</h2>
          <Row className="center-row">
            {sources.map((source, idx) =>
              <Col xs={6} md={4}  className="center-columns">
                <img
                  src={source.logo}
                  alt={source.name}
                  className="logo"
                />
                <p><b>{source.name}</b></p>
                <p>{source.description}</p>
              </Col>
            )}
          </Row>
        </Container>
    );
  }
}

export default class About extends PureComponent {
	render () {
		return (
			<Container id="about-container">
				<Purpose />
				<MemberList />
				<Stats />
        <ProjectLinks />
				<Toolkit />
        <Sources />
        <br/>
        <p>Copyright © 2019 VoteWiselyEngineers</p>
			</Container>
		);
	}
}

export { Purpose, MemberList, Stats, Toolkit };
