import React from 'react';
import GridListTile from '@material-ui/core/GridListTile';
import GridList from '@material-ui/core/GridList';
import { shallow, mount} from 'enzyme';
import {Container, Row, Col} from 'react-bootstrap';


import Banner from '../../components/banner';
import County from '../../components/state-counties';
import Card from '@material-ui/core/Card';
import States from '../states';

describe("State Dialog", () => {
	it("successfully renders without crashing", () => {
		const wrapper = shallow(<States match={{ params: { states: "texas" } }} />);
		expect(
			wrapper.length
		).toEqual(1);
		const grids = wrapper.find(GridList);
		expect(grids).toHaveLength(3);
	});
	it("renders with correct state and finds senators", () => {
		const wrapper = shallow(<States match={{ params: { states: "texas" } }} />);
	
		expect(wrapper.find(GridListTile).length).not.toEqual(0);
		expect(wrapper.find('.flex-section')).toBeDefined();
		expect(wrapper.find(GridList).children()).toHaveLength(3);

	});
	it("also renders appropriate subcomponents of overall component", () => {
		const wrapper = mount(<States match={{ params: { states: "texas" } }} />);
		expect(wrapper.find("img").length).toEqual(1);
		expect(wrapper.find("Row").length).toEqual(1);

	});

	it("check if information for senators is correct", () => {
		const wrapper = mount(<States match={{ params: { states: "texas" } }} />);
		const stateData = [{
			name: "Texas",
			nickname: "Lone Star State",
			governor_name: "Greg Abbott",
			capital: "Austin",
			population: 27862596,
			legislature_name: "Texas Legislature ",
			motto: "Friendship",
			state_flower: "Texas Bluebonnet"
		}];

		const senData = [
		["Ted Cruz","Senator","SenatorTedCruz","@SenTedCruz",""],
		["John Cornyn","Senator","sen.johncornyn","JohnCornyn"]
		];
		wrapper.setState({ stateData: stateData });
		wrapper.setState({ senData: senData });
		const senators_grid = wrapper.find('#grid_state_sen');
		expect(senators_grid.find('h2').at(0).text()).toEqual("Senators");

		const grid_tiles = senators_grid.find(GridListTile);
		expect(grid_tiles.length).toBe(3);
		expect(grid_tiles.at(1).find('img').length).toBe(1);

		const rep_ted_cruz = grid_tiles.at(1);
		expect(rep_ted_cruz.find('h5').at(1).text()).toEqual(senData[0][1]);
		expect(rep_ted_cruz.find('h5').at(0).text()).toEqual(senData[0][0]);

		const rep_john_cornyn = grid_tiles.at(2);
		expect(rep_john_cornyn.find('h5').at(1).text()).toEqual(senData[1][1]);
		expect(rep_john_cornyn.find('h5').at(0).text()).toEqual(senData[1][0]);

	});

	it("check if state info is correct", () => {
		const wrapper = mount(<States match={{ params: { states: "texas" } }} />);
		const stateData = [{
			name: "Texas",
			nickname: "Lone Star State",
			governor_name: "Greg Abbott",
			capital: "Austin",
			population: 27862596,
			legislature_name: "Texas Legislature ",
			motto: "Friendship",
			state_flower: "Texas Bluebonnet"
		}];

		const loaded = true;

		wrapper.setState({ stateData: stateData });
		wrapper.setState({ loaded : loaded});
		const state_info_list = wrapper.find('#profile_block').at(0).find('h5');
		expect(state_info_list.length).toBe(6);
		expect(state_info_list.at(0).text()).toEqual(" Governor: " + stateData[0].governor_name);
		expect(state_info_list.at(1).text()).toEqual(" Capital: " + stateData[0].capital);
		expect(state_info_list.at(2).text()).toEqual(" Population: " + stateData[0].population);
		expect(state_info_list.at(3).text()).toEqual(" Legislature: " + stateData[0].legislature_name);
		expect(state_info_list.at(4).text()).toEqual(" Motto(s): " + stateData[0].motto);
		expect(state_info_list.at(5).text()).toEqual(" State Flower: " + stateData[0].state_flower);

	});

	it("check representative data is correctly rendering", () => {
		const wrapper = mount(<States match={{ params: { states: "texas" } }} />);
		const stateData = [{
			name: "Texas",
			nickname: "Lone Star State",
			governor_name: "Greg Abbott",
			capital: "Austin",
			population: 27862596,
			legislature_name: "Texas Legislature ",
			motto: "Friendship",
			state_flower: "Texas Bluebonnet"
		}];
		const repData = [
		["Jodey Arrington","Representative","JodeyArrington","@RepArrington",""],
		["Joe Barton","Representative","RepJoeBarton","RepJoeBarton",""]
		];
		const loaded = true;

		wrapper.setState({ stateData : stateData})
		wrapper.setState({ repData: repData });
		wrapper.setState({ loaded : loaded});

		const representative_grid = wrapper.find('#grid_state_rep');
		expect(representative_grid.find('h2').at(0).text()).toEqual("Representatives");

		const grid_tiles_temp = representative_grid.find(GridListTile);
		expect(grid_tiles_temp.length).toBe(3);
		expect(grid_tiles_temp.at(1).find('img').length).toBe(1);

		const rep_jodey = grid_tiles_temp.at(1);
		//expect(rep_jodey.find('h5').length).toEqual(2);
		expect(rep_jodey.find('h5').at(0).text()).toEqual(repData[0][0]);
		//expect(rep_jodey.find('h5').at(1).text()).toEqual(repData[0][1]);

		
		const rep_joe = grid_tiles_temp.at(2);
		//expect(rep_joe.find('h5').at(1).text()).toEqual(repData[1][1]);
		expect(rep_joe.find('h5').at(0).text()).toEqual(repData[1][0]);
		
	});

});

