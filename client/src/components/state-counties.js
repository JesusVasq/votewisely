import React, {Component} from 'react';

function getStateCounty (name) {
	switch(name) {
		case 'Alabama':
			return 'https://upload.wikimedia.org/wikipedia/commons/0/00/Alabama_Presidential_Election_Results_2016.svg';
		case 'Alaska':
			return 'https://upload.wikimedia.org/wikipedia/commons/a/ae/Alaska_Presidential_Election_Results_2016.svg';
		case 'Arizona':
			return 'https://upload.wikimedia.org/wikipedia/commons/6/6b/Arizona_Presidential_Election_Results_2016.svg';
		case 'Arkansas':
			return 'https://upload.wikimedia.org/wikipedia/commons/d/d8/Arkansas_Presidential_Election_Results_2016.svg';
		case 'California':
			return 'https://upload.wikimedia.org/wikipedia/commons/5/52/California_Presidential_Election_Results_2016.svg';
		case 'Colorado':
			return 'https://upload.wikimedia.org/wikipedia/commons/0/0e/Colorado_Presidential_Election_Results_2016.svg';
		case 'Connecticut':
			return 'https://upload.wikimedia.org/wikipedia/commons/d/d8/Connecticut_Presidential_Election_Results_2016.svg';
		case 'Delaware':
			return 'https://upload.wikimedia.org/wikipedia/commons/e/e5/Delaware_Presidential_Election_Results_2016.svg';
		case 'Florida':
			return 'https://upload.wikimedia.org/wikipedia/commons/7/7f/Florida_Presidential_Election_Results_2016.svg';
		case 'Georgia':
			return 'https://upload.wikimedia.org/wikipedia/commons/4/42/Georgia_Presidential_Election_Results_2016.svg';
		case 'Hawaii':
			return 'https://upload.wikimedia.org/wikipedia/commons/9/95/Hawaii_Presidential_Election_Results_2016.svg';
		case 'Idaho':
			return 'https://upload.wikimedia.org/wikipedia/commons/8/8c/Idaho_Presidential_Election_Results_2016.svg';
		case 'Illinois':
			return 'https://upload.wikimedia.org/wikipedia/commons/8/82/Illinois_Presidential_Election_Results_2016.svg';
		case 'Indiana':
			return 'https://upload.wikimedia.org/wikipedia/commons/7/76/Indiana_Presidential_Election_Results_2016.svg';
		case 'Iowa':
			return 'https://upload.wikimedia.org/wikipedia/commons/0/0e/Iowa_Presidential_Election_Results_2016.svg';
		case 'Kansas':
			return 'https://upload.wikimedia.org/wikipedia/commons/6/64/Kansas_Presidential_Election_Results_2016.svg';
		case 'Kentucky':
			return 'https://upload.wikimedia.org/wikipedia/commons/d/d9/Kentucky_Presidential_Election_Results_2016.svg';
		case 'Louisiana':
			return 'https://upload.wikimedia.org/wikipedia/commons/d/db/Louisiana_Presidential_Election_Results_2016.svg';
		case 'Maine':
			return 'https://upload.wikimedia.org/wikipedia/commons/a/a2/Maine_Presidential_Election_Results_2016.svg';
		case 'Maryland':
			return 'https://upload.wikimedia.org/wikipedia/commons/5/51/Maryland_Presidential_Election_Results_2016.svg';
		case 'Massachusetts':
			return 'https://upload.wikimedia.org/wikipedia/commons/b/b3/Massachusetts_Presidential_Election_Results_2016.svg';
		case 'Michigan':
			return 'https://upload.wikimedia.org/wikipedia/commons/e/e2/Michigan_Presidential_Election_Results_2016.svg';
		case 'Minnesota':
			return 'https://upload.wikimedia.org/wikipedia/commons/7/7a/Minnesota_Presidential_Election_Results_2016.svg';
		case 'Mississippi':
			return 'https://upload.wikimedia.org/wikipedia/commons/a/aa/Mississippi_Presidential_Election_Results_2016.svg';
		case 'Missouri':
			return 'https://upload.wikimedia.org/wikipedia/commons/7/7e/Missouri_Presidential_Election_Results_2016.svg';
		case 'Montana':
			return 'https://upload.wikimedia.org/wikipedia/commons/3/32/Montana_Presidential_Election_Results_2016.svg';
		case 'Nebraska':
			return 'https://upload.wikimedia.org/wikipedia/commons/2/27/Nebraska_Presidential_Election_Results_2016.svg';
		case 'Nevada':
			return 'https://upload.wikimedia.org/wikipedia/commons/7/79/Nevada_Presidential_Election_Results_2016.svg';
		case 'New Hampshire':
			return 'https://upload.wikimedia.org/wikipedia/commons/f/f9/New_Hampshire_Presidential_Election_Results_2016.svg';
		case 'New Jersey':
			return 'https://upload.wikimedia.org/wikipedia/commons/0/01/New_Jersey_Presidential_Election_Results_2016.svg';
		case 'New Mexico':
			return 'https://upload.wikimedia.org/wikipedia/commons/3/33/New_Mexico_Presidential_Election_Results_2016.svg';
		case 'New York':
			return 'https://upload.wikimedia.org/wikipedia/commons/e/e6/New_York_Presidential_Election_Results_2016.svg';
		case 'North Carolina':
			return 'https://upload.wikimedia.org/wikipedia/commons/8/84/North_Carolina_Presidential_Election_Results_2016.svg';
		case 'North Dakota':
			return 'https://upload.wikimedia.org/wikipedia/commons/2/25/North_Dakota_Presidential_Election_Results_2016.svg';
		case 'Ohio':
			return 'https://upload.wikimedia.org/wikipedia/commons/7/7c/Ohio_Presidential_Election_Results_2016.svg';
		case 'Oklahoma':
			return 'https://upload.wikimedia.org/wikipedia/commons/4/4c/Oklahoma_Presidential_Election_Results_2016.svg';
		case 'Oregon':
			return 'https://upload.wikimedia.org/wikipedia/commons/8/8d/Oregon_Presidential_Election_Results_2016.svg';
		case 'Pennsylvania':
			return 'https://upload.wikimedia.org/wikipedia/commons/d/d7/Pennsylvania_Presidential_Election_Results_2016.svg';
		case 'Rhode Island':
			return 'https://upload.wikimedia.org/wikipedia/commons/7/7d/Rhode_Island_Presidential_Election_Results_2016.svg';
		case 'South Carolina':
			return 'https://upload.wikimedia.org/wikipedia/commons/2/28/South_Carolina_Presidential_Election_Results_2016.svg';
		case 'South Dakota':
			return 'https://upload.wikimedia.org/wikipedia/commons/e/e9/South_Dakota_Presidential_Election_Results_2016.svg';
		case 'Tennessee':
			return 'https://upload.wikimedia.org/wikipedia/commons/7/7b/Tennessee_Presidential_Election_Results_2016.svg';
		case 'Texas':
			return 'https://upload.wikimedia.org/wikipedia/commons/a/a6/Texas_Presidential_Election_Results_2016.svg';
		case 'Utah':
			return 'https://upload.wikimedia.org/wikipedia/commons/4/4d/Utah_Presidential_Election_Results_2016.svg';
		case 'Vermont':
			return 'https://upload.wikimedia.org/wikipedia/commons/2/29/Vermont_Presidential_Election_Results_2016.svg';
		case 'Virginia':
			return 'https://upload.wikimedia.org/wikipedia/commons/8/8e/Virginia_Presidential_Election_Results_2016.svg';
		case 'Washington':
			return 'https://upload.wikimedia.org/wikipedia/commons/8/84/Washington_Presidential_Election_Results_2016.svg';
		case 'West Virginia':
			return 'https://upload.wikimedia.org/wikipedia/commons/7/7d/West_Virginia_Presidential_Election_Results_2016.svg';
		case 'Wisconsin':
			return 'https://upload.wikimedia.org/wikipedia/commons/a/a3/Wisconsin_Presidential_Election_Results_2016.svg';
		case 'Wyoming':
			return 'https://upload.wikimedia.org/wikipedia/commons/c/c0/Wyoming_Presidential_Election_Results_2016.svg';
		default :
			return "TODO: PRINT ERROR MESSAGE";
	}
}

export default class County extends Component {

	render () {
		var StateCountyURL = getStateCounty(this.props.state);

		return (
			<div id="state-county-container">
				<img src={StateCountyURL} id="counties-image" alt="counties-Img"/>
			</div>
		);
	}
}
