import React from 'react';
import { NavLink } from "react-router-dom";
import { Nav } from 'react-bootstrap';
import { shallow } from 'enzyme';

import NavBar from '../navbar';

describe("Navigation Bar", () => {
	it("has links to needed pages", () => {
		const wrapper = shallow(<NavBar />);
		const navs = wrapper.find(Nav);
		expect(navs.length).toEqual(1);
		for(var i = 0; i < navs.length; i++) {
			expect(navs.at(0).containsAllMatchingElements([
				<NavLink exact activeClassName="nav-active" className="navbar-link" to="/">Home</NavLink>,
				<NavLink activeClassName="nav-active" className="navbar-link" to="/about">About</NavLink>,
				<NavLink activeClassName="nav-active" className="navbar-link" to="/issues">Issues</NavLink>,
				<NavLink activeClassName="nav-active" className="navbar-link" to="/politicians">Politicians</NavLink>,
				<NavLink activeClassName="nav-active" className="navbar-link" to="/states">States</NavLink>,
				<NavLink activeClassName="nav-active" className="navbar-link" to="/map">Map</NavLink>
			])).toEqual(true);
		}
	});
});
