import React, {PureComponent} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import '../css/search.css';
import { Link } from 'react-router-dom';


/* Individual search card. Component within SearchResults
   4 Props:           ---------------------------------
    picture           | pic |   Title                 |
    title             |  ^  |   link                  |
    link              |  ^  |   snippet               |
    snippet           ---------------------------------
 */
export default class SearchCard extends PureComponent {
  /* Jay was messing with this */

  parseSnippet(snippet, search, index) {
    var terms = search.split(/\s+/gi).sort((a, b) => b.length - a.length);
    for (var i = 0; i < terms.length; i++) {
      if (terms[i].length > 0) {
        var term = RegExp('(?=(' + terms[i] + '))', 'gi');
        snippet = snippet.replace(term, '<span class="hi-lite">');
        term = RegExp('(?<=(' + terms[i] + '))', 'gi');
        snippet = snippet.replace(term, '</span>');
      }
    }
    return snippet;
  }


  render() {
    return (
      <Container id="searchCard-container">
        <Row>
          <Col md={12}>
            <Row>
              <h4><Link to={this.props.link}>{this.props.title}</Link></h4>
            </Row>
            <Row>
              <span style={{color:"green"}}>https://thewisevote.com{this.props.link}</span>
            </Row>
            <Row>
              <p style={{color:"gray"}}>
                <span dangerouslySetInnerHTML={{
                  __html : this.parseSnippet(String(this.props.snippet), String(this.props.search), 2)
                }}/>
              </p>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}
