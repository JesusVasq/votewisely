import React, {Component} from 'react';
import {Nav} from 'react-bootstrap';
import Button from '@material-ui/core/Button'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { NavLink } from "react-router-dom";
import '../css/navbar.css';
import Image from '../imgs/favicon.ico';

export default class NavBar extends Component {

  constructor(props){
    super(props);
    this.state = {
      openNav: false
    };
  }

  toggleNavbar(open){
    this.setState({openNav: open});
  }

  render() {
    return (
      <div id="navbar-container">

        <Button id="navbar-icon"onClick={(e)=>this.toggleNavbar(true)}>
          <img
            src={Image}
            className="nav-icon-img glowing"
            // style={{height: '80px', width: '80px', borderRadius:'50%'}}
            alt="|||"/>
        </Button>
        <SwipeableDrawer
          open={this.state.openNav}
          classes={{paperAnchorLeft: 'mydrawer'}}
          onOpen={()=>this.toggleNavbar(true)}
          onClose={()=>this.toggleNavbar(false)}
          >
          <div
            tabIndex={0}
            role="button"
            onClick={()=>this.toggleNavbar(false)}
            onKeyDown={()=>this.toggleNavbar(false)}
            >
              <Button className="navbar-button">
                <div className="navbar-link" onClick={(e)=>this.toggleNavbar(false)}>
                  <i className="fas fa-2x fa-times"></i>
                </div>
              </Button><br /><br />
              <Nav>
                <NavLink exact activeClassName="nav-active" className="navbar-link" to="/">Home</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/issues">Issues</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/politicians">Politicians</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/states">States</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/map">Map</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/visualization">Visualization</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/about">About</NavLink>
              </Nav>
          </div>
        </SwipeableDrawer>
      </div>
    );
  }
}
