const express = require('express');
const helpers = require('../lib/sqlHelpers');

const router = express.Router();

const TABLE = 'bills_to_issues';

function bills2issuesSQLGenerator(req) {
  const args = [];
  let sql = `${'SELECT * '
    + 'FROM '}${TABLE}`;
  if (req.query.bill_id) {
    sql += ' WHERE bill_id=?';
    args.push(req.query.bill_id);
  }
  sql += ';';
  return [sql, args];
}

/**
 * Get information that links bills to issues by their id
 * /    ?bill_id="hjres158-103"
 */
router.get('/', helpers.sqlRequestFactory(bills2issuesSQLGenerator));

module.exports = {
  bills2issuesSQLGenerator,
  router,
};
