import React, {PureComponent} from 'react';
import '../css/memberCard.css';
import {Container, Row, Col} from 'react-bootstrap';

export default class MemberCard extends PureComponent {

	render () {
    return (
      <Container id="card" fluid={true}>
        <div id="details">
          <h2>{this.props.name}</h2>
          <h5 style={{color: 'gray', fontStyle: 'italic'}}>{this.props.job}</h5>
					<div className="image-cropper">
	          <img src={this.props.image} id={this.props.picStyle} alt="member-profile"/>
	        </div>
					<Row>
					<Col md="4" xs="4">
						<span style={{color: 'blue'}}>Commits<br/>{this.props.commits}</span>
					</Col>
					<Col md="4" xs="4">
						<span style={{color: 'purple'}}>Issues<br/>{this.props.issues}</span>
					</Col>
					<Col md="4" xs="4">
						<span style={{color: 'red'}}>Tests<br/>{this.props.tests}</span>
					</Col>
					</Row>
					<br/>
					<p>{this.props.class}</p>
          <p id="bio">{this.props.bio}</p>
        </div>
      </Container>
    );
	}
}
