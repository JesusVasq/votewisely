import atexit
import mysql.connector
from mysql.connector import errorcode

config = {
    "user": "votewisely",
    "password": "votewisely123",
    "host": "cs373votewisely.cqqkkpkbowt8.us-east-1.rds.amazonaws.com",
    "port": "3306",
    "database": "VoteWiselyDB",
    "raise_on_warnings": True,
}

try:
    cnx = mysql.connector.connect(**config)
    print("Connection successful!")
    cursor = cnx.cursor()
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
    else:
        print(err)


def sql_exec(statement, args=None):
    try:
        cursor.execute(statement, args)
    except mysql.connector.Error as e:
        print(e.msg)
        return e.errno
    else:
        return 0


def sql_fetch_all(statement):
    sql_exec(statement)
    return cursor.fetchall()


def update_table(table, table_schema, foreign_keys={}):
    if sql_exec(f"DESCRIBE {table}"):
        raise Exception(f"couldn't get {table} table information")
    vals = cursor.fetchall()
    existing_cols = set(col[0] for col in vals)
    for col, desc in table_schema.items():
        if not col in existing_cols:
            sql_exec(f'ALTER TABLE {table} ADD {col} {" ".join(desc)};')
        else:
            sql_exec(f'ALTER TABLE {table} MODIFY {col} {" ".join(desc)};')
    for col in vals:
        if foreign_keys and col[3] and str(col[3]) == "MUL":
            sql_exec(f"ALTER TABLE {table} DROP FOREIGN KEY {col[0]}")
        if not col[0] in table_schema:
            sql_exec(f"ALTER TABLE {table} DROP COLUMN {col[0]}")
    for fk in foreign_keys:
        sql_exec(
            f"ALTER TABLE {table} ADD FOREIGN KEY ({fk}) REFERENCES {foreign_keys[fk]}"
        )
    cnx.commit()
    print(f"Updated {table}")


def create_table(table, table_schema, foreign_keys={}, table_constraints={}):
    nl = ",\n"
    table_description = (
        f"CREATE TABLE {table} "
        f"({nl.join([col + ' ' + ' '.join(desc) for col, desc in table_schema.items()])}"
    )
    table_description += (
        f", {nl.join(['CONSTRAINT ' + name + ' ' + desc for name, desc in table_constraints.items()])});"
        if table_constraints
        else ");"
    )
    print(f"Create table {table}")
    err = sql_exec(table_description)
    if err == errorcode.ER_TABLE_EXISTS_ERROR:
        print("already exists.")
        update_table(table, table_schema, foreign_keys)
    else:
        cnx.commit()
        print("OK")


def insert_table(table, table_schema, values, primary_key=None):
    add_row = (
        f"INSERT INTO {table} "
        f"({', '.join([k for k in values if k in table_schema])}) "
        f"VALUES ({', '.join(['%('+ k +')s' for k in values if k in table_schema])})"
    )
    existing_values = []
    if primary_key:
        if sql_exec(f"SELECT {primary_key} FROM {table}"):
            raise Exception(f"couldn't get {table} table information")
        existing_values = [val[0] for val in cursor.fetchall()]
    if values.get(primary_key, None) in existing_values:
        update_row = (
            f"UPDATE {table} "
            f"SET {', '.join([col + '=%(' + col + ')s' for col in values if col in table_schema])} "
            f"WHERE {primary_key}=%({primary_key})s"
        )
        sql_exec(update_row, values)
        print(f"Update of {values[primary_key]} completed!")
    else:
        sql_exec(add_row, values)
        print(
            f"Insertion of {values[primary_key] if primary_key in values else next(iter(values.values()))} completed!"
        )
    cnx.commit()


def close_all():
    print("Closing connections...")
    cursor.close()
    cnx.close()


atexit.register(close_all)
