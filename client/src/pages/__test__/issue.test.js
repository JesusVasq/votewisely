import React from 'react';
import { Link } from 'react-router-dom';
import { shallow } from 'enzyme';

import Issue from '../issue';

describe("Issue page", () => {
	it("renders loading initially", () => {
		const wrapper = shallow(<Issue match={{ params: { issue: "test" } }}/>);

		const loadingSections = wrapper.find('h3');
		expect(loadingSections.length).toEqual(3);
		for (var i = 0; i < loadingSections.length; i++)
			expect(loadingSections.at(i).text()).toEqual('Loading...');
		const noneSections = wrapper.find('p');
		expect(noneSections.length).toEqual(2);
		for (var i = 0; i < noneSections.length; i++)
			expect(noneSections.at(i).text()).toEqual('None');
	});

	it('will scroll to the top upon update component', () => {
		const wrapper = shallow(<Issue match={{ params: { issue: "test" } }}/>);
		window.scrollTo = jest.fn(); // jsDOM did not implement window.scrollTo
		wrapper.setState({ issueData: {} });

		expect(window.scrollTo.mock.calls.length).toEqual(1);
		expect(window.scrollTo.mock.calls[0]).toEqual([0, 0]);
	});

	it("renders description correctly", () => {
		const wrapper = shallow(<Issue match={{ params: { issue: "test" } }}/>);
		const issueData = {
			summary: "p1\np2",
			src: "test_src",
			img1: "test_img"
		};
		window.scrollTo = jest.fn();
		wrapper.setState({ issueData: issueData });

		const descriptions = wrapper.find('#description').find('p');
		expect(descriptions.length).toEqual(3);
		expect(descriptions.at(0).text()).toEqual('p1');
		expect(descriptions.at(1).text()).toEqual('p2');
		expect(descriptions.at(2).find('a').first().prop('href')).toEqual(issueData.src);
		expect(wrapper.find('#description').find('img').prop('alt')).toEqual(issueData.img1);
	});

	it('render question correctly', () => {
		const wrapper = shallow(<Issue match={{ params: { issue: "test" } }}/>);
		const testQuestion = {
			question: "Should the government classify Bitcoin as a legal currency?",
			side_yes: [{comment: "Yes", perc: 35}],
			side_no: [{comment: "No", perc: 65}],
			pie: "test-pie"
		};
		window.scrollTo = jest.fn();
		wrapper.setState({ issueData: testQuestion });

		const questions = wrapper.find('#question');
		expect(questions.find('p').length).toEqual(7);
		expect(questions.find('p').at(0).text()).toEqual('Should the government classify Bitcoin as a legal currency?');
		expect(questions.find('p').at(1).text()).toEqual('Yes');
		expect(questions.find('p').at(2).text()).toEqual('35%');
		expect(questions.find('p').at(3).text()).toEqual('No');
		expect(questions.find('p').at(4).text()).toEqual('65%');
		expect(wrapper.find('#second-image').find('img').prop('alt')).toEqual(testQuestion.pie);
	});

	it('render category correctly', () => {
		const wrapper = shallow(<Issue match={{ params: { issue: "test" } }}/>);
		const testCategory = {
			category: "test-category",
		};
		window.scrollTo = jest.fn();
		wrapper.setState({ issueData: testCategory });

		const questions = wrapper.find('#category');
		expect(questions.find('p').length).toEqual(1);
		expect(questions.find('p').at(0).text()).toEqual('Category: test-category');
	});

	it('render statements correctly', () => {
		const wrapper = shallow(<Issue match={{ params: { issue: "test" } }}/>);
		const testStatements = [{
			url: "test-url1",
			title: "a",
			name: "b",
			date: "c",
			state: "TX"
		}, {
			url: "test-url2",
			title: "d",
			name: "e",
			date: "f",
			state: "SD"
		}];
		window.scrollTo = jest.fn();
		wrapper.setState({ statements: testStatements });

		const statements = wrapper.find('#statements');
		expect(statements.find('p').length).toEqual(4);
		expect(statements.find('p').at(0).text()).toEqual('a by <Link />on c in <Link />');
		expect(statements.find('p').at(0).find(Link).at(0).prop('to')).toEqual('/politicians/b');
		expect(statements.find('p').at(0).find(Link).at(1).prop('to')).toEqual('/states/texas');
		expect(statements.find('p').at(1).text()).toEqual('d\xA0by <Link />on f in\xA0<Link />');
		expect(statements.find('p').at(1).find(Link).at(0).prop('to')).toEqual('/politicians/e');
		expect(statements.find('p').at(1).find(Link).at(1).prop('to')).toEqual('/states/south-dakota');
	});

	it('render bills correctly', () => {
		const wrapper = shallow(<Issue match={{ params: { issue: "test" } }}/>);
		const testBills = [{
			congressdotgov_url: "test-url1",
			title: "a",
			sponsor_name: "b",
			sponsor_party: "D",
			sponsor_state: "TX"
		}, {
			congressdotgov_url: "test-url2",
			title: "c",
			sponsor_name: "d",
			sponsor_party: "R",
			sponsor_state: "SD"
		}];
		window.scrollTo = jest.fn();
		wrapper.setState({ bills: testBills });

		const bills = wrapper.find('#bills');
		expect(bills.find('p').length).toEqual(4);
		expect(bills.find('p').at(0).text()).toEqual('a\xA0By <Link />(Democrat) from\xA0<Link />');
		expect(bills.find('p').at(0).find(Link).at(0).prop('to')).toEqual('/politicians/b');
		expect(bills.find('p').at(0).find(Link).at(1).prop('to')).toEqual('/states/texas');
		expect(bills.find('p').at(1).text()).toEqual('c\xA0By <Link />(Republican) from\xA0<Link />');
		expect(bills.find('p').at(1).find(Link).at(0).prop('to')).toEqual('/politicians/d');
		expect(bills.find('p').at(1).find(Link).at(1).prop('to')).toEqual('/states/south-dakota');
	});
});
