import React, {PureComponent} from 'react';
import SearchCard from './searchCard';
import '../css/search.css';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { nodelink } from '../api';

export default class SearchResults extends PureComponent {

  page = 0;


  state = {
    query: this.props.query,
    searchPull: [],
    searchData: [],
    results: 0,
    anchor: null,
    mobile: false,
    loaded: false,
  };

  applySearch(){
    fetch(nodelink + "/api/search/" + this.props.type
                   + "?&p=" + this.page
                   + "&q=" + this.props.query)
			.then(res => res.json())
      .then(res => res.response)
      .then(res =>{
        this.setState({searchPull: res[0]})
        this.setState({results: res[1][0].elements})
        this.setData()
      });
  }

  setData(){
    var tempData = this.state.searchData;
    for(var i = (this.page * 10); i < this.state.results; i++){
      var dataSlot=[];
      if(i - (this.page * 10) < (this.state.searchPull.length)){
        dataSlot[0] = this.state.searchPull[i%10].title; //route / link
        dataSlot[1] = this.state.searchPull[i%10].route; //title
        dataSlot[2] = this.state.searchPull[i%10].search_string; //snippet
      } else{
        dataSlot[0] = "route/link"; //route / link
        dataSlot[1] = "TITLE"; //title
        dataSlot[2] = "<span>snippet</span>"; //snippet
      }
      tempData[i] = dataSlot;
    }
    this.setState({searchData: tempData});
    this.setState({loaded: true});
  }

  componentDidMount() {
		this.applySearch();
	}

  componentDidUpdate(prevProps, prevState){
      if(this.props.query !== prevProps.query){
        var temp = [];
        this.setState({searchData: temp});
        this.setState({loaded: false});
        this.applySearch();
      }
  }

  getMuiTheme = () => createMuiTheme({
    overrides: {
      MUIDataTableBodyCell: {
        root: {
          border: "none"
        }
      },
      MUIDataTableToolbar: {
        root: {
        }
      },
      MuiTableHead: {
        root: {
          display: "none"
        }
      },
    }
  })

  columns = [
    {
      name: "title",
      label: "hi",
      options:{
        filter: false,
        customBodyRender: (value, tableMeta, updateValue) => {
					return (
						<SearchCard
              title={value}
              link={tableMeta.rowData[1]}
              snippet={tableMeta.rowData[2]}
              search={this.props.query}
						/>
					);
				},
      }
    },
    {
      name: "link",
      label: " ",
      options:{
        filter: false,
        display: 'excluded',
      }
    },
    {
      name: "snippet",
      label: " ",
      options:{
        filter: false,
        display: 'excluded',
      }
    },
  ];

  options = {
    page: this.page,
    rowsPerPage: 10,
    rowsPerPageOptions: 10,
    customToolbar: null,
    sort: false,
    filter: false,
    print: false,
    download: false,
    search: false,
    viewColumns: false,
    selectableRows: false,
    elevation: 0,
    rowHover: false,
    responsive: 0,
    onChangePage: (currentPage) => this.onChangePage(currentPage)
  };

  onChangePage(page){
    this.setState({
      loaded: false,
    });
    this.page = page;
    this.applySearch();
    window.scrollTo(0, 0);
  }

  getCount(){
    if(this.state.loaded){
      return this.state.results
    }
  }

  renderData(){
    if(this.state.loaded){
      this.options.page = this.page;
      return(
        <div id="search-table">
          <MuiThemeProvider theme={this.getMuiTheme()}>
            <MUIDataTable
              title=<span>Search results for "{this.props.query}"</span>
              data={this.state.searchData}
              columns={this.columns}
              options={this.options}
              id="state-table"
            />
          </MuiThemeProvider>
        </div>
      );
    }
  }

  render() {
    return (
      <div id="searchResults-container">
        {this.renderData()}
      </div>
    );
  }
}
