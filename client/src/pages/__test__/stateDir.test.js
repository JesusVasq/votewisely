import React from 'react';
import MUIDataTable from 'mui-datatables';
import { mount, shallow } from 'enzyme';
import {Container, Row, Col} from 'react-bootstrap';


import StatesDir from '../statesDir';

describe("States Directory", () => {
	it("successfully renders without crashing", () => {
		const wrapper = shallow(<StatesDir />);
		expect(
			wrapper.length
		).toEqual(1);
	});
	it("renders with states table date and has a title", () => {
		const wrapper = mount(<StatesDir />);

		expect(wrapper.find("img").length).toEqual(2);
		expect(wrapper.find("img").at(0).prop('src')).toEqual("https://elevenews.com/wp-content/uploads/2018/08/us-government.jpg");
		expect(wrapper.find("img").at(0).prop('id')).toEqual("statesdir-bg");

		/* MUIDataTable moved into renderTable method */
		// expect(wrapper.find(MUIDataTable).exists()).toEqual(true);
	});
});
