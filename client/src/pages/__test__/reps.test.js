import GridListTile from '@material-ui/core/GridListTile';
import { shallow, mount} from 'enzyme';

import Banner from '../../components/banner';

import Representatives from '../representatives';

import React, {PureComponent} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
/* Material UI GridList */
import Grid from '@material-ui/core/Grid';
import GridList from '@material-ui/core/GridList';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
/* Material UI Card */
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

/* Social/Icons */
import {TwitterTimelineEmbed} from 'react-twitter-embed';
import { BrowserRouter as Router } from 'react-router-dom';

describe("Single Representative render", () => {
	it("successfully renders without crashing", () => {
		const wrapper = shallow(<Representatives match={{ params: { reps: "ted-cruz" } }} />);
		expect(
			wrapper.length
		).toEqual(1);
	});

	it("renders with correct viable representative data", () => {
		const wrapper = mount(<Representatives match={{ params: { reps: "ted-cruz" } }} />);
		expect(wrapper.children().length).toEqual(1);
		expect(wrapper.find("Row").length).toEqual(2);
		expect(wrapper.find("Col").length).toEqual(6);
		expect(wrapper.find('.flex-section')).toBeDefined();
	});

	it("makes sure all both related people and bills are rendered", () => {
		const wrapper = mount(<Representatives match={{ params: { reps: "ted-cruz" } }} />);
		expect(wrapper.find("Container").length).toEqual(3);
	});

	it("check if representative info is correct", () => {
		const wrapper = mount(<Representatives match={{ params: { reps: "ted-cruz" } }} />);
		const senList = [{
			twitter_account: "test_twittter",
			facebook_account: "test_facebook",
			id: "test_id",
			state_fullname: "test_full_namen",
			first_name: "test_first_name",
			last_name: "test_last_name ",
			img_url: "test_img",
			state: "test_state",
			date_of_birth: "test_dob",
			party:"test_party",
			phone:"phone",
			office: "office"
		}];

		const allLoaded = true;

		wrapper.setState({ senList: senList });
		wrapper.setState({ allLoaded : allLoaded});

		const state_info_list = wrapper.find("CardMedia");
		expect(state_info_list.length).toBe(1);
		expect(state_info_list.prop('image')).toEqual(senList[0].img_url);
		expect(state_info_list.prop('title')).toEqual(senList[0].first_name);

		const card_content = wrapper.find("CardContent");
		expect(card_content.find('Col').length).toEqual(3);


		/*for(var i = 0; i < 3; i ++){
			expect(card_content.find('Col').at(i).find('a').length).toEqual(1);
		}*/
		// expect(state_info_list.length).toBe(6);
		// expect(state_info_list.at(0).text()).toEqual(" Governor: " + stateData[0].governor_name);
		// expect(state_info_list.at(1).text()).toEqual(" Capital: " + stateData[0].capital);
		// expect(state_info_list.at(2).text()).toEqual(" Population: " + stateData[0].population);
		// expect(state_info_list.at(3).text()).toEqual(" Legislature: " + stateData[0].legislature_name);
		// expect(state_info_list.at(4).text()).toEqual(" Motto(s): " + stateData[0].motto);
		// expect(state_info_list.at(5).text()).toEqual(" State Flower: " + stateData[0].state_flower);

	});


});

describe("Twitter Box renders ", () => {
	it("checks if twitter box renders", () => {
		const wrapper = shallow(<TwitterTimelineEmbed
					sourceType="profile"
					screenName="SenTedCruz"
					options={{height: 400}}
					id="twitter-timeline"/>);
		expect(
			wrapper.length
		).toEqual(1);
	});

});
