const express = require('express');
const helpers = require('../lib/sqlHelpers');

const router = express.Router();

const TABLE = 'states_table';

function stateInfoSQLGenerator(req) {
  const args = [req.params.abbrev];
  const sql = `${'SELECT * '
    + 'FROM '}${TABLE
  } WHERE abbrev=?;`;
  return [sql, args];
}

/* Request example: /api/states/TX */
/* Returns information on a specific state with an abbreviation */
router.get('/:abbrev', helpers.sqlRequestFactory(stateInfoSQLGenerator));

function statesSQLGenerator() {
  const args = [];
  const sql = `${'SELECT * '
    + 'FROM '}${TABLE};`;
  return [sql, args];
}

/* Return information about all states */
/* Request example: /api/states  */
router.get('/', helpers.sqlRequestFactory(statesSQLGenerator));

module.exports = {
  stateInfoSQLGenerator,
  statesSQLGenerator,
  router,
};
