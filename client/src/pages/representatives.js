import React, {PureComponent} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import '../css/representative.css'
import {nodelink} from '../api';
/* Material UI GridList */
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
/* Material UI Card */
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';

/* Social/Icons */
import {TwitterTimelineEmbed} from 'react-twitter-embed';

/* ExpansionPanel */
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import DefaultPic from '../imgs/missingPicture.jpg';


function titleCase(str) {
   var splitStr = str.toLowerCase().split(' ');
   for (var i = 0; i < splitStr.length; i++) {
       // You do not need to check if i is larger than splitStr length, as your for does that for you
       // Assign it back to the array
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
   }
   // Directly return the joined string
   return splitStr.join(' ');
}

export default class Representatives extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			senList: [],
			loaded: false,
			relPeople: [],
			issues_info: [],
			allList: [],
			allLoaded: false,
			allData: [],
			stateBills: [],
			billsData: [],
			allBillIssues:[],
			billsLoaded: false,
			billsToIssues: [],
      width: window.innerWidth,
      columns: 0,
      expanded: null,
    }
	}

  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  // make sure to remove the listener
  // when the component is not mounted anymore
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
    if(this.state.width <= 480)
      this.setState({columns: 2});
    else if(this.state.width <= 768)
      this.setState({columns: 3});
    else
      this.setState({columns: 4});
  };

	componentDidMount() {
		var name = String(this.props.match.params.reps).split("-");
		var first = name[0].charAt(0).toUpperCase() + name[0].slice(1);
		var last = name[1].charAt(0).toUpperCase() + name[1].slice(1);
		this.setState({RepName: first + " " + last});
		fetch(nodelink + "/api/reps/senate")
			.then(res => res.json())
			.then(res => {
				this.setState({allList: res.response })
			});
		fetch(nodelink + "/api/reps/?first_name=" + first +"&last_name=" + last)
			.then(res => res.json())
			.then(res => {
				this.setState({ senList: res.response })
				this.setState({ loaded: true });})
			.then(() => {
				fetch(nodelink + "/api/bills2/?sponsor_id=" + this.state.senList[0].id)
					.then(r => r.json())
					.then(r => r.response)
					.then(r => this.setState({stateBills: r}))
					.then(r => 
						fetch(nodelink + "/api/issues2")
						.then(res => res.json())
						.then(res => res.response)
						.then(res => this.setState({issues_info: res[0]}))					
					)
					.then(r =>
						fetch(nodelink + "/api/bills2issues")
						.then(rs => rs.json())
						.then(rs => rs.response)
						.then(rs => this.setState({billsToIssues: rs}))
						.then(rs => this.setState({allBillIssues: this.setAllBillsData()}))
					)
					.then(r => this.setBillData(this.state.stateBills))
					.then(r => this.setState({billsLoaded: true}));


		   	fetch(nodelink + "/api/reps/house")
				.then(res => res.json())
				.then(res => {
			   		this.setState({ repList: res.response });
			        this.getAllData();
					this.setState({allLoaded: true});
					this.getRelPeople(this.state.allLoaded);
			      });
			});

      this.handleWindowSizeChange();
	}

	setAllBillsData(){
		var issues_for_all_bills = [];
		for(var i = 0; i < this.state.stateBills.length; i++){
			var current_bill_id = this.state.stateBills[i].bill_id;
			var current_bill_issues = this.getIssueByBill(current_bill_id);
			issues_for_all_bills.push(current_bill_issues);
		}
		return issues_for_all_bills;
	}

	getIssuePic(issue_name){
		for(var i = 0; i < this.state.issues_info.length; i++){
			if(issue_name === this.state.issues_info[i].route){
				var img_link = this.state.issues_info[i].icon;
				return img_link;
			}
		}
		return "";
	}

	getIssueByBill(billId){
		var issues = [];
		for(var i = 0; i < this.state.billsToIssues.length; i ++){
			var tempBill  = this.state.billsToIssues[i];
			if(tempBill.bill_id === billId){
				var issue_name = tempBill.issue;
				issue_name = titleCase(issue_name.replace("_"," "));
				issues.push(tempBill.issue);
			}
		}
		return issues;
	}


  setBillData(pulledData){
		var extData = [];
		for(var i = 0; i < pulledData.length; i++){
			var tempData = []
			var BillID = pulledData[i].bill_id;
			var Title = pulledData[i].title;
			var IssueName = this.state.allBillIssues[i][0];
			var BillURL = pulledData[i].congressdotgov_url;
			tempData[0] = BillID;
			tempData[1] = Title;
			tempData[2] = IssueName;
			tempData[3] = BillURL;
			tempData[4] = this.getIssuePic(IssueName);
			extData[i] = tempData;
		}
		this.setState({billsData : extData})
	}


	getAllData() {
		var repData2 = this.state.repList;
		for (var i = 0; i < this.state.allList.length; i++) {
			repData2.push(this.state.allList[i]);
		}
		this.setState({allData: repData2});
	}

	renderBill(loaded) {
		if(this.state.billsLoaded)

			return (
				<GridList id="grid_state_bill" cellHeight={150} cols={this.state.columns}>
					{this.state.billsData.map(tile => (
						<GridListTile key={tile[0]}>
							<img src={tile[4]} alt={tile[0]}/>
							<GridListTileBar
								title={<h5 className="title">{tile[1]}</h5>}
								subtitle={<a href={"/issues/"+tile[2]}><u style={{color: 'white'}}><h5 className="subtitle">{titleCase(tile[2].replace("_"," "))}</h5></u></a>}
								actionIcon={
									<IconButton color="primary" href={tile[3]}>
										<InfoIcon />
									</IconButton>
								}
							/>
						</GridListTile>
					))}

				</GridList>
			);
	}

	renderTwitterFeed () {
		if(this.state.loaded){
			return (
				<TwitterTimelineEmbed
					sourceType="profile"
					screenName={this.state.senList[0].twitter_account}
					options={{height: 'calc(100vh - 80px)'}}
					id="twitter-timeline"
				/>
			);
    }
	}

	profileStyle = {
		height: '300px'
	}

	convertDate(date) {
		var temp = date.split('-');
		return(temp[1] + '-' + temp[2] + '-' + temp[0]);
	}

	getParty(party) {
		if(party === 'D') return "Democrat";
		else if (party === 'R') return "Republican";
		else return "Other"
	}

	getStateName() {
		var result = this.state.senList[0].state_fullname;
		result = result.replace(/\s+/g, '-').toLowerCase();
		return result;
	}

	renderProfile() {
    var isNull = ((this.state.senList[0] === undefined) ? true : false);

    var hasFB = ((!isNull) ? this.state.senList[0].facebook_account : null);
    var hasYT = ((!isNull) ? this.state.senList[0].youtube_account : null);
    var hasTW = ((!isNull) ? this.state.senList[0].twitter_account : null);

			return (
				<Card>
					<CardHeader
						title = {<h2 id="name">{
              (!isNull) ?
                this.state.senList[0].first_name + " " + this.state.senList[0].last_name
                : "Name"
            }</h2>}
						subheader = {<h4>{!isNull ? this.state.senList[0].title : "Title"}</h4>}
					/>
					<CardMedia
	          image={!isNull ? this.state.senList[0].img_url : DefaultPic}
	          title={!isNull ? this.state.senList[0].first_name : ''}
						style={this.profileStyle}
	        />
					<CardContent>
          <Container>
						<Row>
							<Col xs={4} md={4}>
								{(!isNull && hasFB) ?
									<a
										href={"https://www.facebook.com/" + this.state.senList[0].facebook_account}
										rel="noopener noreferrer" target="_blank"
										>
										<i className={"fa-3x fab fa-facebook-square social-" + hasFB} />
									</a> :
									<i className={"fa-3x fab fa-facebook-square social-" + hasFB} />
								}
              </Col>
							<Col xs={4} md={4}>
								{(!isNull && hasYT) ?
									<a
										href={"https://youtube.com/" + this.state.senList[0].youtube_account}
										disabled={(!isNull && hasFB)}
										rel="noopener noreferrer" target="_blank"
										>
										<i className={"fa-3x fab fa-youtube social-" + hasYT} />
									</a> :
									<i className={"fa-3x fab fa-youtube social-" + hasYT} />
								}
              </Col>
							<Col xs={4} md={4}>
								{(!isNull && hasTW) ?
									<a
										href={"https://twitter.com/" + this.state.senList[0].twitter_account}
										disabled={(!isNull && hasFB)}
										rel="noopener noreferrer" target="_blank"
										>
										<i className={"fa-3x fab fa-twitter-square social-" + hasTW} />
									</a> :
									<i className={"fa-3x fab fa-twitter-square social-" + hasTW} />
								}

              </Col>
						</Row>
						<br />
            <Container>
            	<a href={!isNull ? "/states/" + this.getStateName() : '/states/'}>
                <h5>
                  State: {!isNull ? this.state.senList[0].state : "State"}
                </h5></a>
							<h5> Party: {!isNull ? this.getParty(this.state.senList[0].party): 'Party'}</h5>
							<h5> Phone: {!isNull ? this.state.senList[0].phone: '123-456-7890'}</h5>
							<h5> Office: {!isNull ? this.state.senList[0].office: 'Unavailable'}</h5>
							<h5><i className="fa-lg fas fa-birthday-cake" />  :
                {!isNull ? this.convertDate(this.state.senList[0].date_of_birth) : 'DOB'}
              </h5>
						</Container>
          </Container>
        </CardContent>
				</Card>
			);
	}

	getRelPeople(allLoaded) {
		if (this.state.allLoaded) {
		var temp = []
		var myState = this.state.senList[0].state;
		var myId = this.state.senList[0].id;
		if (true) {
			for (var i = 0; i < this.state.allData.length; i++) {
				var tempState = this.state.allData[i].state;
				var tempId = this.state.allData[i].id;
				if (tempState === myState && tempId !== myId) {
					temp.push(this.state.allData[i]);
				}
			}
			this.setState({relPeople: temp});
		}
	}
	}

	renderRelatedPeople(allLoaded) {
		if(this.state.relPeople.length > 0)
			return (
				<div>
				<GridList cellHeight={200} cols={this.state.columns}>
				{this.state.relPeople.map(person => (
					<GridListTile key={person.img_url}>
						<img
							src={person.img_url}
							alt=""/>
						<GridListTileBar
							title={<h5 className="title">{person.first_name} {person.last_name}</h5>}
							actionIcon={
								<IconButton
                  color="primary"
                  className="RelPeopleLink"
                  href={"/politicians/"+person.first_name+ "-"+ person.last_name}>
									<InfoIcon />
								</IconButton>
							}
						/>
					</GridListTile>
					))}
					</GridList>
					</div>
			);
	}

  bioStyle={
    background: 'white',
    textAlign: 'left',
    lineHeight: '1.8em',
    color: 'gray',
    padding: '15px',
    borderRadius: '5px',
    boxShadow: '0 0 5px lightgray'
  }

  renderSummary() {
    if(this.state.allLoaded){
      return(
        <Container id="bio-container" style={this.bioStyle}>
          <h3>Short Bio</h3>
          {this.state.senList[0].bio}
          <br />
          <a href={this.state.senList[0].wiki_url} rel="noopener noreferrer" target="_blank">Read more</a>
        </Container>
      );
    }
  }

  /* Expansion Panel Functions */
  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };


	render() {

		return (
			<Container className="flex-section" style={{height: '100%'}}>
				<Row >
					<Col xs={12} md={3} className="flex-col-scroll" id="section-profile">
						{this.renderProfile()}
					</Col>
					<Col xs={12} md={6} className="flex-col-scroll" id="section-newsfeed">
            {this.renderSummary()}
            <br />
            <ExpansionPanel
              expanded={this.state.expanded === 'panel1'}
              onChange={this.handleChange('panel1')}
              style={{borderRadius: '5px'}}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <h2>Bills Sponsored</h2>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
              <div>{this.renderBill()}</div>
              </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel
              id="panel-related"
              expanded={this.state.expanded === 'panel2'}
              onChange={this.handleChange('panel2')}
              style={{borderRadius: '5px'}}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <h2>Related People</h2>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <div>{this.renderRelatedPeople(this.state.allLoaded)}</div>
              </ExpansionPanelDetails>
            </ExpansionPanel>
					</Col>
          <Col xs={12} md={3} className="flex-col-scroll" id="section-twitter">
            {this.renderTwitterFeed(this.state.loaded)}
          </Col>
				</Row>
			</Container>
		);

	}
}
